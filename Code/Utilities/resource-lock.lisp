(in-package :decentralise-utilities)

;;; A vector of "shared" locks, for approximately locking independent
;;; resources without creating a lock for each of them.

;;; The obvious downside of this vector is that we can "alias" nested
;;; locks. Would some names A and B hash to the same lock, and L is a
;;; lock vector, then (with-named-resource (A L) (with-named-resource
;;; (B L) ...)) would result in attempting to grab the lock
;;; recursively. Some uses are instead supported by
;;; WITH-NAMED-RESOURCES where we can write
;;; (with-named-resources ((list A B) L) ...) if we know we want to lock
;;; A and B at the same time.

;;; We could instead use a recursive lock, but then allowing nesting
;;; like this could lead to deadlock which also isn't cool. As such,
;;; it is a "feature" that we don't support nested locks.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defconstant +table-size+ 256))
(deftype lock-table ()
  `(simple-array bt:lock (256)))

(defmacro with-named-resource ((name lock-table &key &allow-other-keys) &body body)
  "Run BODY when no other thread has access to NAME."
  (alexandria:once-only (name)
    `(bt:with-lock-held ((svref ,lock-table
                                (mod (sxhash ,name) +table-size+)))
       ,@body)))

(defun make-named-resource-table (&key (name "Some resource lock"))
  (let ((table (make-array +table-size+)))
    (dotimes (n +table-size+)
      (setf (svref table n)
            (bt:make-lock name)))
    table))

(declaim (inline call-with-named-resources))
(defun call-with-named-resources (names lock-table continuation)
  (declare (optimize (speed 3))
           (function continuation)
           (lock-table lock-table)
           (list names))
  ;; n.b. we use a byte array rather than a bit array to avoid
  ;; excess shifting. Most computers only have byte addressing and
  ;; not bit addressing. Unless "most computers" are x86-64 and thus
  ;; have the BT and BTS instructions...well, most computers are,
  ;; but the instructions are not much faster than shifting
  ;; yourself, and no compiler uses them.
  (let ((acquire-table (make-array +table-size+
                                   :element-type '(unsigned-byte 8)
                                   :initial-element 0)))
    (declare (dynamic-extent acquire-table))
    ;; Set bits for each segment we need to acquire.
    (dolist (name names)
      (setf (aref acquire-table (mod (sxhash name) +table-size+)) 1))
    ;; Acquire every lock from the table.
    ;; As we always lock in a specific order, a non-nested call to
    ;; CALL-WITH-NAMED-RESOURCES won't deadlock.
    (loop for bit across acquire-table
          for position of-type (mod #.+table-size+) from 0
          when (= 1 bit)
            do (bt:acquire-lock (aref lock-table position)))
    (unwind-protect
         (funcall continuation)
      ;; Release every lock from the table.
      (loop for bit across acquire-table
            for position of-type (mod #.+table-size+) from 0
            when (= 1 bit)
              do (bt:release-lock (aref lock-table position))))))

(defmacro with-named-resources ((names lock-table) &body body)
  "Run BODY when no other thread has access to any of NAMES."
  `(call-with-named-resources ,names ,lock-table (lambda () ,@body)))
