(defpackage :decentralise-binary-syntax
  (:use :cl :decentralise-utilities)
  (:export #:write-short-integer #:read-short-integer
           #:write-short-string #:read-short-string
           #:write-long-integer #:read-long-integer
           #:*maximum-vector-length*
           #:write-long-bytes #:read-long-bytes
           #:write-long-string #:read-long-string
           #:write-listing-data #:read-listing-data
           #:write-node-data #:read-node-date
           #:with-output-to-byte-array
           #:map-binary-listing #:map-binary-nodes))
(in-package :decentralise-binary-syntax)

(deftype consumer () '(function ((unsigned-byte 8))))
(deftype producer () '(function () (unsigned-byte 8)))
(deftype byte-vector () '(simple-array (unsigned-byte 8) (*)))

;;; Short integers (bytes)
(declaim (inline read-short-integer write-short-integer)
         (optimize (speed 3) (safety 1)))
(defun write-short-integer (integer function)
  "Write a 8-bit unsigned integer."
  (declare ((unsigned-byte 8) integer)
           (consumer function))
  (assert (<= 0 integer 255))
  (funcall function integer))
(defun read-short-integer (function)
  "Read a 8-bit unsigned integer."
  (declare (producer function))
  (funcall function))

;;; Short strings
(declaim (inline write-short-string read-short-string))
(defun write-short-string (string function)
  "Write a short string (a string with less than 2^8 characters)."
  (let ((bytes (babel:string-to-octets string :encoding :utf-8)))
    (declare (byte-vector bytes))
    (write-short-integer (length bytes) function)
    (loop for byte across bytes do (funcall function byte))))
(defun read-short-string (function)
  "Read a short string."
  (let* ((length (read-short-integer function))
         (bytes  (make-array length :element-type '(unsigned-byte 8))))
    (declare (dynamic-extent bytes))
    (dotimes (n length)
      (setf (aref bytes n) (funcall function)))
    (babel:octets-to-string bytes :encoding :utf-8)))

;;; Long integers
(declaim (inline write-long-integer read-long-integer))
(defun write-long-integer (integer function)
  "Write a 64-bit unsigned integer."
  (declare ((unsigned-byte 64) integer)
           (consumer function))
  (macrolet ((make ()
               `(progn
                  ,@(loop for offset from 56 downto 0 by 8
                          collect `(funcall function (ldb (byte 8 ,offset) integer))))))
    (make)))
(defun read-long-integer (function)
  "Read a 64-bit signed integer."
  (declare (producer function))
  (let ((integer 0))
    (declare ((unsigned-byte 64) integer))
    (macrolet ((make ()
                 `(progn
                    ,@(loop for offset from 56 downto 0 by 8
                            collect `(setf integer (dpb (funcall function)
                                                        (byte 8 ,offset)
                                                        integer))))))
      (make))))

;;; Long byte vectors
(defvar *maximum-vector-length* 10000000
  "The maximum length of a vector that we want to read.
Setting this too high could let someone convince us to allocate a vector that is too large for our heap, which would be disastrous.")

(declaim (inline write-long-bytes read-long-bytes))
(defun write-long-bytes (bytes function)
  "Write a long byte vector (up to 2^64 bytes)."
  (declare (consumer function)
           (byte-vector bytes))
  (write-long-integer (length bytes) function)
  (loop for byte across bytes do (funcall function byte)))
(defun read-long-bytes (function)
  "Read a long byte vector."
  (let ((length (read-long-integer function)))
    (assert (<= length *maximum-vector-length*)
            ()
            "We were sent a ~d byte vector, but we only want to read up to ~d byte vectors."
            length *maximum-vector-length*)
    (let ((bytes (make-array length :element-type '(unsigned-byte 8))))
      (dotimes (n length)
        (setf (aref bytes n) (funcall function)))
      bytes)))

;;; Long strings
(declaim (inline write-short-string read-short-string))
(defun write-long-string (string function)
  "Write a short string (a string with less than 2^8 characters)."
  (let ((bytes (babel:string-to-octets string :encoding :utf-8)))
    (declare (byte-vector bytes))
    (write-long-bytes bytes function)))
(defun read-long-string (function)
  "Read a short string."
  (babel:octets-to-string (read-long-bytes function)
                          :encoding :utf-8))

;;; Listing data
(declaim (inline write-listing-data read-listing-data))
(defun write-listing-data (name version channels function)
  (declare (string name)
           ((unsigned-byte 64) version)
           (list channels))
  (write-short-string name function)
  (write-long-integer version function)
  (write-long-integer (length channels) function)
  (dolist (channel channels)
    (write-short-string channel function)))

(defun read-listing-data (function)
  (let* ((name    (read-short-string function))
         (version (read-long-integer function))
         (channel-count (read-long-integer function))
         (channels (loop repeat channel-count
                         collect (read-short-string function))))
    (list name version channels)))

;;; Node data
(defun write-node-data (uri node-id function)
  (write-short-string uri function)
  (write-short-string node-id function))
(defun read-node-data (function)
  (list (read-short-string function) (read-short-string function)))

(defconstant +chunk-size+ 1024)
(defmacro with-output-to-byte-array ((consumer &key (chunk-size +chunk-size+))
                                     &body body)
  (alexandria:with-gensyms (chunk-count chunks current-chunk position)
    `(let ((,position 0)
           (,chunk-count 0)
           (,current-chunk (make-array ,chunk-size
                                       :element-type '(unsigned-byte 8)))
           (,chunks '()))
       (declare ((integer 0 ,chunk-size) ,position)
                ((integer 0 ,(floor most-positive-fixnum chunk-size))
                 ,chunk-count)
                (byte-vector ,current-chunk))
       (flet ((,consumer (byte)
                (setf (aref ,current-chunk ,position) byte)
                (incf ,position)
                (when (= ,position ,chunk-size)
                  (push ,current-chunk ,chunks)
                  (incf ,chunk-count)
                  (setf ,current-chunk
                        (make-array ,chunk-size
                                    :element-type '(unsigned-byte 8))
                        ,position 0))))
         (declare (inline ,consumer))
         ,@body
         (let* ((final-size (+ ,position (* ,chunk-count ,chunk-size)))
                (byte-vector (make-array final-size
                                        :element-type '(unsigned-byte 8)))
                (position 0))
           (declare (fixnum final-size position))
           (dolist (chunk (nreverse ,chunks))
             (declare (byte-vector chunk))
             (dotimes (offset ,chunk-size)
               (setf (aref byte-vector (+ offset position))
                     (aref chunk offset)))
             (incf position ,chunk-size))
           (dotimes (offset ,position)
             (setf (aref byte-vector (+ offset position))
                   (aref ,current-chunk offset)))
           byte-vector)))))

(defun map-binary-listing (function data)
  (declare (byte-vector data))
  (let ((position 0))
    (flet ((producer ()
             (prog1 (aref data position)
               (incf position))))
      (loop until (= position (length data))
            do (apply function (read-listing-data #'producer))))))
(defun map-binary-nodes (function data)
  (declare (byte-vector data))
  (let ((position 0))
    (flet ((producer ()
             (prog1 (aref data position)
               (incf position))))
      (loop until (= position (length data))
            do (apply function (read-node-data #'producer))))))
