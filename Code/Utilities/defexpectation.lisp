(in-package :decentralise-utilities)

(defmacro defexpectation (name lambda-list)
  "A consise way to check types on a generic function.
Expects a lambda-list made of just variables, or (specifier type).

A specifier can be:
- a variable. This just checks if the variable is typep to the type.
- (every VARIABLE). This checks the type against every item of the list
  VARIABLE."
  (let ((lambda-list (loop for item in lambda-list
                           collect (if (listp item) item (list item t)))))
    `(defmethod ,name :before ,(loop for (variable type) in lambda-list
                                     collect (etypecase variable
                                               (list
                                                (ecase (first variable)
                                                  ((every)
                                                   (destructuring-bind (value) (rest variable)
                                                     value))))
                                               (symbol variable)))
       . ,(loop for (variable type) in lambda-list
                unless (eql type 't)
                collect (etypecase variable
                          (list
                           (ecase (first variable)
                             ((every)
                              (destructuring-bind (value) (rest variable)
                                `(progn
                                   (check-type ,value list)
                                   (dolist (item ,value)
                                     (check-type item ,type)))))))
                          (symbol `(check-type ,variable ,type)))))))
