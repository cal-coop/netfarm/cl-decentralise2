(in-package :decentralise-utilities)

(defmacro with-thread ((&key name) &body body)
  `(bt:make-thread
    (lambda ()
      . ,body)
    :name ,name))
