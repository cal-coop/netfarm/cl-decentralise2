(in-package :decentralise-utilities)

(defmacro doit (&body body)
  "Macroexpand this code at compile time."
  (alexandria:with-gensyms (name)
    `(macrolet ((,name () ,@body))
       (,name))))

(defmacro dolistit ((&rest bindings) &body body)
  "Macroexpand this code, with each element of BINDINGS destructured as (NAME &REST VALUES), with each NAME substituted for each item of VALUES at compile time."
  (let ((bindings* (apply #'mapcar #'list bindings)))
    (destructuring-bind (names &rest values) bindings*
      `(progn
         ,@(loop for value-list in values
                 collecting `(symbol-macrolet ,(mapcar #'list names value-list)
                               ,@body))))))
        
