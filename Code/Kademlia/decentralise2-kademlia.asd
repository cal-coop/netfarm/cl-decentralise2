(asdf:defsystem :decentralise2-kademlia
  :depends-on (:decentralise2-systems :decentralise2-client
               :cl-heap :trivial-timeout)
  :components ((:file "package")
               (:file "system-mixin")
               (:file "scheduler")
               (:module "Client"
                :components ((:file "classes")
                             (:file "client")
                             (:file "protocol")
                             (:file "search")))))
