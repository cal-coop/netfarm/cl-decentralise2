(in-package :decentralise-kademlia)

(defmethod decentralise-client:client-subclients ((client kademlia-client))
  (kademlia-client-subclients client))

(defmethod decentralise-client:client-has-block-p
    ((client kademlia-client) name)
  (loop for subclient-information in (kademlia-client-subclients client)
        for subclient = (client-information-client subclient-information)
        thereis (decentralise-client:client-has-block-p subclient name)))

(defmethod decentralise-client:map-subclients
    (function (client kademlia-client) message-name)
  (let ((name (second message-name))
        (attempts 3)
        (writing? (eql (first message-name)
                       :block)))
    (decentralise-client:functional-action
     (lambda (success failure)
       (labels ((not-found ()
                  (funcall failure
                           (make-condition 'decentralise-client:no-applicable-subclients
                                           :client client
                                           :name message-name)))
                (ensure-client ()
                  (if (zerop attempts)
                      (not-found)
                      (progn
                        (decf attempts)
                        (do-after ((kademlia-client-scheduler client) 0)
                          (ensure-client-with-name-is-reachable
                           client name
                           #'actually-map-subclients
                           failure
                           writing?)))))
                (actually-map-subclients ()
                  (let ((subclients (compute-applicable-subclients
                                     client name
                                     :writing? writing?)))
                    (%map-subclients function client
                                     subclients
                                     success failure #'ensure-client))))
         (call-after (kademlia-client-scheduler client) 0 #'ensure-client))))))
