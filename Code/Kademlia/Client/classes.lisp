(in-package :decentralise-kademlia)


;;; The Kademlia client is quite big, possibly because we attempt to query
;;; subclients concurrently and with some fault tolerance.

;;; We store some information about both URIs we can try to connect to later,
;;; and extra information about clients, such as what URI we used to create it.
(defclass uri-information ()
  ((uri :initarg :uri
        :initform (alexandria:required-argument :uri)
        :reader uri-information-uri)
   (hash :initarg :hash
         :initform (alexandria:required-argument :hash)
         :reader uri-information-hash)))
(defmethod print-object ((uri uri-information) stream)
  (print-unreadable-object (uri stream :type t)
    (write-string (uri-information-uri uri) stream)))

(defun uri-information= (uri1 uri2)
  (and (= (uri-information-hash uri1)
          (uri-information-hash uri2))
       (string= (uri-information-uri uri1)
                (uri-information-uri uri2))))

(defclass client-information ()
  ((client :initarg :client
           :initform (alexandria:required-argument :client)
           :reader client-information-client)
   (uri    :initarg :uri
           :initform (alexandria:required-argument :uri)
           :reader client-information-uri)
   (requests :initform 0
             :accessor client-information-requests)
   (successful-requests :initform 0
                        :accessor client-information-successful-requests)))

(defclass kademlia-client (decentralise-client:client kademlia-mixin)
  ((channels :initform '("all")
             :initarg :channels
             :reader kademlia-client-channels)
   (clients :initform (box '() :copier #'copy-list)
            :accessor %kademlia-client-clients
            :documentation "The current subclient information objects of this client.")
   (concurrent-requests :initarg :concurrent-requests
                        :initform 3
                        :reader kademlia-client-concurrent-requests
                        :documentation "The number of requests this client can make at a time.")
   (known-uris :initform (box '() :copier #'copy-list)
               :reader %kademlia-client-known-uris
               :documentation "A box with the URIs that this client can retrieve later.")
   (subclient-timeout :initarg :subclient-timeout
                      :initform 10
                      :reader kademlia-client-subclient-timeout
                      :documentation "The time, in seconds, that the client will wait for a subclient to respond.")
   (subclient-class :initarg :subclient-class
                    :initform 'decentralise-client:connection-client
                    :reader kademlia-client-subclient-class
                    :documentation "The class of subclients that the client should create.") 
   (scheduler :initform (make-instance 'scheduler)
              :reader kademlia-client-scheduler)
   (searcher :reader kademlia-client-searcher))
  (:documentation "A client that can find subclients and retrieve blocks from systems that subclass KADEMLIA-SYSTEM."))

(defmethod initialize-instance :after ((client kademlia-client)
                                       &key (worker-threads 1)
                                            (threads-per-search 3)
                                            (bootstrap-uris '())
                                            (bootstrap-connections '()))
  (setf (slot-value client 'searcher)
        (make-instance 'searcher
                       :client client
                       :threads-per-search threads-per-search))
  (start-scheduler (kademlia-client-scheduler client)
                   :threads worker-threads)
  (dolist (uri bootstrap-uris)
    (add-subclient client uri))
  (dolist (connection bootstrap-connections)
    (let ((subclient (make-instance (kademlia-client-subclient-class client)
                                    :connection connection
                                    :channels (kademlia-client-channels client)
                                    :should-get-system-info t)))
      (add-subclient-information client
                                 (make-instance 'uri-information
                                                :uri ""
                                                :hash nil)
                                 subclient))))

(defmethod decentralise-client:stop-client :after ((client kademlia-client))
  (dolist (subclient-information (kademlia-client-subclients client))
    (decentralise-client:stop-client
     (client-information-client subclient-information)))
  (stop-scheduler (kademlia-client-scheduler client))
  (fail-everything (kademlia-client-searcher client)))
