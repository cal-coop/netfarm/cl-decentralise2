(in-package :decentralise-kademlia)

;;; Box accessor macros. Maybe these should go in Utilities/
(defmacro define-with-box (with-name accessor-name box-value-name)
  `(defmacro ,with-name ((,box-value-name client) &body body)
     `(decentralise-utilities:with-unlocked-box
          (,,box-value-name (,',accessor-name ,client))
        ,@body)))
(defmacro with-box-value ((box) &body body)
  `(decentralise-utilities:with-unlocked-box (,box ,box)
     ,@body))
(define-with-box with-subclients %kademlia-client-clients clients)
(define-with-box with-known-uris %kademlia-client-known-uris uris)

(defun kademlia-client-subclients (client)
  (box-value (%kademlia-client-clients client)))
(defun kademlia-client-known-uris (client)
  (box-value (%kademlia-client-known-uris client)))

(defgeneric add-subclient-information (client uri subclient)
  (:method ((client kademlia-client) uri subclient)
    (let* ((node-data (decentralise-client:get-data subclient "nodes"
                                                    :timeout 10))
           (information (make-instance 'client-information
                                       :client subclient
                                       :uri (uri-information-uri uri))))
      (push (lambda (connection)
              (declare (ignore connection))
              (remove-subclient client subclient))
            (decentralise-connection:connection-destructors
             (decentralise-client:client-connection subclient)))
      (with-subclients (clients client)
        (push information clients)
        (with-known-uris (uris client)
          (setf uris (remove uri uris
                             :test #'uri-information=))
          (map-nodes (lambda (uri id)
                       (let ((hash (parse-hash-name client id)))
                         (assert (not (null hash)))
                         ;; Ensure we haven't already connected to this URI,
                         ;; and that we aren't intending to connect, before
                         ;; adding it again.
                         (unless (or (member uri clients
                                             :key #'client-information-uri
                                             :test #'string=)
                                     (member uri uris
                                             :key #'uri-information-uri
                                             :test #'string=))
                           (let ((uri (make-instance 'uri-information
                                                     :hash hash
                                                     :uri uri)))
                             (push uri uris))
                           (with-unlocked-box (jobs
                                               (searcher-jobs (kademlia-client-searcher client)))
                             (dolist (job jobs)
                               (add-node-information-to-job job uri hash))))))
                     node-data)))
      (run-search-hooks client))))

(defgeneric add-subclient (client uri &optional hash)
  (:method ((client kademlia-client) uri &optional (hash nil))
    (let ((subclient (decentralise-client:connect-to-uri uri
                      :class (kademlia-client-subclient-class client)
                      :channels (kademlia-client-channels client)
                      :should-get-system-info t)))
      (add-subclient-information client (make-instance 'uri-information
                                                       :uri uri
                                                       :hash hash)
                                 subclient))))
(defun add-connection-from-uri (client uri)
  (add-subclient client uri))

(defgeneric remove-subclient (client subclient)
  (:documentation "Remove a subclient from a Kademlia client.")
  (:method ((client kademlia-client) subclient)
    (with-subclients (clients client)
      (setf clients (remove subclient clients)))))

(defgeneric sort-known-uris (client hash)
  (:method ((client kademlia-client) hash)
    (let ((uris (kademlia-client-known-uris client)))
      (sort uris #'<
            :key (lambda (uri)
                   (logxor hash (uri-information-hash uri)))))))

(defgeneric compute-applicable-subclients (client name &key writing?)
  (:method ((client kademlia-client) name &key writing?)
    (with-subclients (clients client)
      (loop for client-information in clients
            for client = (client-information-client client-information)
            for version = (decentralise-client:client-has-block-p client name)
            unless (and (not writing?) (null version))
              collect (cons client version) into clients
            finally (return (mapcar #'car
                                    (sort clients #'> :key #'cdr)))))))

(defun %map-subclients (function client subclients %succeed %fail give-up)
  (let* ((done? (box nil))
         (subclients (safe-queue:make-queue :initial-contents subclients))
         (searchers (min (safe-queue:queue-count subclients)
                         (kademlia-client-concurrent-requests client)))
         (remaining-searchers (box searchers)))
    (labels ((handle-end-of-search ()
               ;; Only give up when no other searchers are running.
               (with-box-value (remaining-searchers)
                 (decf remaining-searchers)
                 (when (zerop remaining-searchers)
                   (call-after (kademlia-client-scheduler client) 0 give-up))))
             (succeed (value)
               (with-box-value (done?)
                 (setf done? t))
               (funcall %succeed value))
             (fail (condition)
               (with-box-value (done?)
                 (setf done? t))
               (funcall %fail condition))
             (next-subclient ()
               (call-after (kademlia-client-scheduler client)
                           0 #'work-step))
             (work-step ()
               (with-box-value (done?)
                 (when done?
                   (return-from work-step)))
               (when (safe-queue:queue-empty-p subclients)
                 (handle-end-of-search)
                 (return-from work-step))
               (multiple-value-bind (subclient subclient?)
                   (safe-queue:dequeue subclients)
                 (unless subclient?
                   (return-from work-step (handle-end-of-search)))
                 (handler-bind ((error (lambda (c)
                                         (remove-subclient client subclient)
                                         (fail c))))
                   (funcall function subclient #'succeed #'fail #'next-subclient))
                 (call-after (kademlia-client-scheduler client)
                             (kademlia-client-subclient-timeout client)
                             #'work-step))))
      (when (zerop searchers)
        (handle-end-of-search))
      (dotimes (n searchers)
        (call-after (kademlia-client-scheduler client) 0 #'work-step)))))

(defmethod decentralise-client:count-known-blocks ((client kademlia-client))
  "Admittedly somewhat approximate."
  (with-subclients (clients client)
    (reduce #'+ clients
            :key (alexandria:compose #'decentralise-client:count-known-blocks
                                     #'client-information-client))))
