(in-package :decentralise-kademlia)

;;; Searching for new connections using the Kademlia client is handled using
;;; a kind of dynamic thread pool, instead of the simple scheduler used for
;;; searching. This is because creating connections in decentralise2 is
;;; synchronous (to allow MAKE-INSTANCE and signalling conditions to work
;;; normally), unlike the connection messaging system.

;;; All the information required to fulfil a job is stored in a SEARCH-JOB
;;; object, including the name of the block we are searching for, a priority
;;; queue of URI information objects to look through, the callbacks we want to
;;; invoke when we succeed or fail searching, and some stuff we would need to
;;; maintain thread pools and to update the priority queue with new URI
;;; information.
(defstruct search-job
  uri-queue name test distance-function
  success-continuation failure-continuation
  (done? (box nil :copier #'identity))
  (running-threads (box 0 :copier #'identity)))

(defstruct search-hook
  test
  action)

(defclass searcher ()
  ((threads-per-search :initform 3
                       :initarg :threads-per-search
                       :reader searcher-threads-per-search
                       :documentation "The number of threads that should be delegated to a search.")
   (connections :initform (box 0 :copier #'identity)
                :reader %searcher-connections
                :documentation "The number of connections being made.")
   (running-jobs :initform (box '())
                 :reader searcher-jobs
                 :documentation "A list of all the search jobs running.")
   (client :initarg :client :reader searcher-client))
  (:documentation "An object that contains all the state for searching nodes in a Kademlia world on behalf of a Kademlia client."))

(defmethod initialize-instance :after ((searcher searcher) &key)
  (assert (plusp (searcher-threads-per-search searcher))))

(defun try-to-connect-to (searcher uri-information)
  (let ((uri  (uri-information-uri uri-information))
        (hash (uri-information-hash uri-information))
        (client (searcher-client searcher)))
    (with-subclients (clients client)
      (when (member uri clients
                    :key #'client-information-uri
                    :test #'string=)
        ;; Avoid connecting to a client twice.
        (return-from try-to-connect-to)))
    (ignore-errors
     (add-subclient client uri hash))))

(defvar *initial-backoff-time* 0.001)
(defun search-for-client (searcher job)
  (loop with backoff-time = *initial-backoff-time*
        ;; Behold, a valid FOR = ... form!
        for nil = (progn
                    (when (box-value (search-job-done? job))
                      (return-from search-for-client nil))
                    (with-unlocked-box (connections (%searcher-connections searcher))
                      (incf connections)))
        for uri-information = (with-unlocked-box (queue (search-job-uri-queue job))
                                (cl-heap:dequeue queue))
        if (null uri-information)
          ;; No new URIs; wait a bit and try again.
          do (with-unlocked-box (connections (%searcher-connections searcher))
               (decf connections))
             ;; We did just bump the count of connections, even though
             ;; we have no connection yet. This might have convinced
             ;; another searcher thread to not fail searching, even
             ;; though it should.
             (maybe-fail (searcher-client searcher) searcher)
             (sleep backoff-time)
             (setf backoff-time (* backoff-time 2))
        else
          do (setf backoff-time *initial-backoff-time*)
             (try-to-connect-to searcher uri-information)
             (with-unlocked-box (connections (%searcher-connections searcher))
               (decf connections))
             (maybe-fail (searcher-client searcher) searcher)))

(defun %fail-job (job)
  (setf (box-value (search-job-done? job)) t)
  (funcall (search-job-failure-continuation job)))
(defun succeed-job (job searcher)
  (with-unlocked-box (done? (search-job-done? job))
    (unless done?
      (setf done? t)
      (funcall (search-job-success-continuation job))
      (with-unlocked-box (jobs (searcher-jobs searcher))
        (setf jobs (remove job jobs))))))

(defun fail-everything (searcher)
  (with-unlocked-box (jobs (searcher-jobs searcher))
    (mapc #'%fail-job jobs)))

(defun maybe-fail (client searcher)
  ;; Fail when we're the last thread around, and there are no more URIs.
  (when (and (zerop (box-value (%searcher-connections searcher)))
             (with-unlocked-box (uris (%kademlia-client-known-uris client))
               (null uris)))
    (with-unlocked-box (jobs (searcher-jobs searcher))
      (mapc #'%fail-job jobs)
      (setf jobs '()))))

(defun run-search-hooks (client)
  (let ((succeeded-jobs '()))
    (with-unlocked-box (jobs
                        (searcher-jobs (kademlia-client-searcher client)))
      (loop for job in jobs
            when (funcall (search-job-test job))
              do (push job succeeded-jobs)
            else
              collect job into jobs-to-keep
            finally (setf jobs jobs-to-keep)))
    (dolist (job succeeded-jobs)
      (succeed-job job (kademlia-client-searcher client))))
  (maybe-fail client (kademlia-client-searcher client)))

(defun add-search-job (searcher job)
  (with-unlocked-box (jobs
                      (searcher-jobs searcher))
    (push job jobs)
    (dotimes (i (searcher-threads-per-search searcher))
      (with-thread (:name "Kademlia search thread")
        (search-for-client searcher job)))))

(defgeneric ensure-client-with-name-is-reachable (client name success fail writing?)
  (:documentation "Ensure that the client can reach the block with given name, then call the success continuation.
If the object cannot be reached, call the failure continuation with an appropriate condition.")
  (:method :around ((client kademlia-client) name success fail writing?)
    ;; Don't do anything if the name is reachable.
    (if (null (compute-applicable-subclients client name :writing? writing?))
        (call-next-method)
        (funcall success)))
  (:method ((client kademlia-client) name success fail writing?)
    (let ((hash  (parse-hash-name client name))
          (queue (make-instance 'cl-heap:priority-queue)))
      (when (or (null hash)
                (null (kademlia-client-known-uris client)))
        (funcall fail (make-condition 'simple-error
                                      :format-control "Can't parse the hash name ~s"
                                      :format-arguments (list name)))
        (return-from ensure-client-with-name-is-reachable))
      (flet ((distance-function (uri-hash)
               (logxor uri-hash hash))
             (fail ()
               (funcall fail (make-condition 'decentralise-client:remote-error
                                             :name name
                                             :message "not found"))))
        (dolist (uri-info (kademlia-client-known-uris client))
          (cl-heap:enqueue queue
                           uri-info
                           (distance-function (uri-information-hash uri-info))))
        (add-search-job (kademlia-client-searcher client)
                        (make-search-job
                         :test (lambda ()
                                 (not (null (compute-applicable-subclients client name
                                                                           :writing? writing?))))
                         :distance-function #'distance-function
                         :uri-queue (box queue)
                         :name name
                         :success-continuation success
                         :failure-continuation #'fail))))))

(defun add-node-information-to-job (job uri hash)
  (with-unlocked-box (queue (search-job-uri-queue job))
    (cl-heap:enqueue queue
                     (make-instance 'uri-information
                                    :uri uri
                                    :hash hash)
                     (funcall (search-job-distance-function job)
                              hash))))
