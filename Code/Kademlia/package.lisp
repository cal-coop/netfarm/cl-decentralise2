(defpackage :decentralise-kademlia
  (:use :cl :decentralise-utilities)
  (:export #:parse-hash-name
           #:kademlia-mixin #:interesting-name-p
           #:kademlia-client
           #:kademlia-client-known-uris
           #:kademlia-system-mixin
           #:kademlia-system-integer-distance
           #:add-connection-from-uri
           #:add-subclient-information
           #:remove-subclient))
