(asdf:defsystem :decentralise2
  :author "Cooperative of Applied Language"
  :depends-on (:decentralise2-acceptors :decentralise2-connections
               :decentralise2-client :decentralise2-kademlia
               :decentralise2-messages :decentralise2-systems)
  :license "Cooperative Software License of 2020-11-08+"
  :version "0.1.0")
