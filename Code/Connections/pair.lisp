(in-package :decentralise-connection)

(defclass passing-connection (connection)
  ((base-class :reader passing-connection-base-class)
   (target-connection :initarg :target :accessor passing-connection-target)
   (message-mailbox :initform (safe-queue:make-mailbox)
                    :reader passing-connection-mailbox)
   (name :initarg :name :initform "Passing-Connection" :reader passing-connection-name)
   (direction :initarg :direction :initform nil :reader passing-connection-direction)
   (worker-thread :accessor passing-connection-thread))
  (:documentation "Pass all messages sent to this connection to the TARGET. Two of these linked to each other work like a socket."))

(defun change-class-by-target-element-type (connection)
  (let ((type (if (slot-boundp connection 'target-connection)
                  (connection-element-type connection)
                  t)))
    (change-class connection (passing-connection-base-class connection))
    (alexandria:switch (type :test #'subtypep)
      ('(unsigned-byte 8)
       (dynamic-mixins:ensure-mix connection
                                  'binary-connection))
      ('character
       (dynamic-mixins:ensure-mix connection
                                  'character-connection))
      ('t
       (dynamic-mixins:ensure-mix connection
                                  'binary-connection
                                  'character-connection)))))

(defmethod (setf passing-connection-target) :after
    (new-target (connection passing-connection))
  (unless (slot-boundp connection 'base-class)
    (change-class-by-target-element-type connection)))
                                               
(defmethod print-object ((connection passing-connection) stream)
  (print-unreadable-object (connection stream :type t)
    (format stream ":Name ~s :Direction ~s"
            (passing-connection-name connection)
            (passing-connection-direction connection))))

(defmethod connection-element-type ((connection passing-connection))
  (connection-element-type (passing-connection-target connection)))

(defun passing-connection-thread-loop (connection)
  (lambda ()
    (loop with mailbox = (passing-connection-mailbox connection)
          for nil = (when (connection-stopped-p connection)
                      (return))
          for messages = (safe-queue:mailbox-receive-message mailbox
                                                             :timeout 1)
          do (dolist (message messages)
               (handle-message (passing-connection-target connection) message))
          #+sbcl (dolist (messages
                          (safe-queue:mailbox-receive-pending-messages mailbox))
                   (dolist (message messages)
                     (handle-message (passing-connection-target connection) message))))))

(defmethod initialize-instance :after ((connection passing-connection) &key)
  ;; We will change classes to a dynamic mixin, so we should preserve the
  ;; original class.
  (setf (slot-value connection 'base-class) (class-of connection))
  (change-class-by-target-element-type connection)
  (setf (passing-connection-thread connection)
        (bt:make-thread (passing-connection-thread-loop connection)
                        :name (format nil "~s worker thread" connection))))

(defmethod stop-connection progn ((connection passing-connection))
  (stop-connection (passing-connection-target connection)))

(defun handle-messages-on-passing-connection (connection messages)
  (safe-queue:mailbox-send-message (passing-connection-mailbox connection)
                                   messages))

#+trace-passing-connection
(defvar *trace-lock* (bt:make-lock))
(defmethod write-message ((connection passing-connection) message)
  #+trace-passing-connection
  (bt:with-lock-held (*trace-lock*)
    (format *debug-io* "~&~16@a ~d=> ~s"
            (passing-connection-name connection)
            (passing-connection-direction connection)
            message))
  (handle-messages-on-passing-connection connection (list message)))
(defmethod write-messages ((connection passing-connection) messages)
  (handle-messages-on-passing-connection connection messages))

(defun make-hidden-socket (&key (class 'passing-connection)
                                (name "hidden socket")
                                (initargs '()))
  "Return two PASSING-CONNECTIONs that work like a socket, sending messages to each other.  
If CLASS is provided, that class will be instantiated instead. 
NAME is the name of the PASSING-CONNECTIONs created.
See PASSING-CONNECTION."
  (let ((connection1 (apply #'make-instance class :name name :direction 1 initargs))
        (connection2 (apply #'make-instance class :name name :direction 2 initargs)))
    (setf (passing-connection-target connection1) connection2
          (passing-connection-target connection2) connection1)
    (values connection1 connection2)))
