(in-package :decentralise-connection)

(defun read-boolean (stream)
  (string= (read-word stream) "yes"))
(defun write-boolean (boolean stream)
  (write-string (if boolean "yes" "no") stream))

(defmacro with-syntax-type ((name reader writer) part &body body)
  (let ((base-name (gensym "BASE")))
    `(destructuring-bind (,name ,base-name) ,part
       (declare (ignorable ,name))
       (let ((,reader (alexandria:symbolicate "READ-" ,base-name))
             (,writer (alexandria:symbolicate "WRITE-" ,base-name)))
         (declare (ignorable ,reader ,writer))
         ,@body))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun variable-list (parts)
    (loop for part in parts
          unless (eql part '&rest)
            collect (first part))))

(defmacro define-syntax (class &body syntax)
  "Define all the messaging-related methods for a class.
 A syntax entry looks like (:verb-name (\"first-word\" (part type) ...
                                                       [&rest (part-list type)])
                                       body ...)"
  `(progn
     ;; Create writer methods
     ,@(loop with object = (gensym "OBJECT")
             for (verb-name (first-word . parts) . body) in syntax
             for accessors = (decentralise-messages:message-accessors verb-name)
             for type = (decentralise-messages:message-type-specifier verb-name)
             collect `(defmethod write-message
                          ((connection ,class) (,object ,type))
                        (with-connection-stream (stream connection)
                          (write-string ,(concatenate 'string first-word " ")
                                        stream)
                          ,@(loop for (part . rest) on parts
                                  for accessor in accessors
                                  collect (cond
                                            ((eql part '&rest)
                                             (with-syntax-type (name reader writer)
                                                               (first rest)
                                               `(loop for (part . rest) on (,accessor ,object)
                                                      do (,writer part stream)
                                                      unless (null rest)
                                                        do (write-char #\Space stream))))
                                            ((stringp part)
                                             `(write-string ,part stream))
                                            (t
                                             (with-syntax-type (name reader writer)
                                                               part
                                               `(,writer (,accessor ,object) stream))))
                                  when (eql part '&rest)
                                    do (loop-finish)
                                  unless (null rest)
                                    collect `(write-char #\Space stream))
                          (terpri stream))))
       
       ;; Create reader method
       (defmethod read-message ((connection ,class))
         (let* ((stream (connection-stream connection))
                (first-word (read-word stream)))
           (cond
             ,@(loop for (verb-name (first-word . parts) . body) in syntax
                     for variables = (variable-list parts)
                     collect `((string= ,first-word first-word)
                               (let ,(loop for (part . rest) on parts
                                           if (eql part '&rest)
                                             collect (with-syntax-type (name reader writer)
                                                                       (first rest)
                                                       `(,name (loop do (drop-spaces stream)
                                                                     if (char= (peek-char nil stream)
                                                                               #\Newline)
                                                                       do (read-char stream)
                                                                          (loop-finish)
                                                                     else
                                                                       collect (,reader stream))))
                                             and do (loop-finish)
                                           else
                                             collect (with-syntax-type (name reader writer)
                                                                       part
                                                       `(,name (progn (drop-spaces stream)
                                                                      (,reader stream)))))
                                 ,@(if (null body)
                                       `((decentralise-messages:message ,verb-name ,@variables))
                                       body)))))))))
