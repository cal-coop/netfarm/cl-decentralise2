(asdf:defsystem :decentralise2-connections
  :depends-on (:usocket :cl+ssl :dynamic-mixins :split-sequence
               :decentralise2-utilities :decentralise2-messages
               :safe-queue)
  :serial t
  :components ((:file "package")
               (:module "Protocol"
                :components ((:file "class")
                             (:file "messages")
                             (:file "element-types")
                             (:file "uri")))
               (:file "pair")
               (:file "threaded")
               (:file "socketed")
               (:file "define-syntax")
               (:file "netfarm-wire-format")
               (:file "netfarm-binary-format")))
