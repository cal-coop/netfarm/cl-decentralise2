(in-package :decentralise-connection)

(defclass threaded-connection (connection)
  ((reader-thread)
   (writer-thread)
   (outgoing-message-mailbox
    :initform (safe-queue:make-mailbox :name "THREADED-CONNECTION message mailbox")
    :reader outgoing-message-mailbox))
  (:documentation "A connection that uses a reader thread to read messages and handle them, and a writer thread to write messages without blocking the client."))

(defgeneric read-message (connection)
  (:documentation "Read a message from a connection."))

(defgeneric reader-loop (connection)
  (:method ((connection threaded-connection))
    (loop
      (when (connection-stopped-p connection)
        (return-from reader-loop))
      (multiple-value-bind (message done?)
          (read-message connection)
        (when done?
          (return-from reader-loop))
        (handle-message connection message)))))

(defvar *in-writer-loop* nil)
(defgeneric writer-loop (connection)
  (:method ((connection threaded-connection))
    (let ((mailbox (outgoing-message-mailbox connection)))
      (loop
        (when (connection-stopped-p connection)
          (return-from writer-loop))
        (let ((message (safe-queue:mailbox-receive-message mailbox
                                                           :timeout 0.1)))
          (unless (null message)
            (let ((*in-writer-loop* t))
              (handler-bind ((error (lambda (e)
                                      (declare (ignore e))
                                      (stop-connection connection))))
                (write-message connection message)))))))))

(defmethod write-message :around ((connection threaded-connection) message)
  (if *in-writer-loop*
      (call-next-method)
      ;; Enqueue the message to be sent by the writer thread.
      (safe-queue:mailbox-send-message (outgoing-message-mailbox connection)
                                       message)))

(defmethod initialize-instance :after ((connection threaded-connection) &key)
  (let ((name (class-name (class-of connection))))
    (setf (slot-value connection 'reader-thread)
          (with-thread (:name (format nil "~s reader thread" name))
            (reader-loop connection))
          (slot-value connection 'writer-thread)
          (with-thread (:name (format nil "~s writer thread" name))
            (writer-loop connection)))))
