(defpackage :decentralise-connection
  (:use :cl :decentralise-utilities :decentralise-binary-syntax :split-sequence)
  (:export #:connection #:threaded-connection
           #:socketed-connection #:connection-socket
           #:character-connection #:binary-connection
           #:netfarm-format-connection #:netfarm-binary-connection
           #:passing-connection #:passing-connection-target
           #:passing-connection-direction #:make-hidden-socket
           #:stop-connection
           #:connection-message-handler
           #:with-errors-blamed-on-connection
           #:read-message #:write-message #:write-messages
           #:protocol-creator #:define-protocol
           #:connection-from-uri
           #:connection-uri #:connection-uri-protocol-name
           #:connection-address
           #:write-block #:request-block #:request-blocks
           #:write-ok #:write-error
           #:subscribe #:write-subscription
           #:announce-node #:allow-announcement
           #:connection-channels #:connection-destructors
           #:connection-id
           #:connection-stopped-p #:connection-announcing-p
           #:connection-element-type
	   #:connection-counter-value #:with-connection-counter))
