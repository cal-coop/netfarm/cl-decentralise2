(in-package :decentralise-connection)

(defclass connection ()
  ((announcing :initform nil :accessor connection-announcing-p)
   (destructors :initform '() :accessor connection-destructors)
   (id :initform nil :accessor connection-id)
   (message-handler :initarg :message-handler
                    :accessor connection-message-handler)
   (stopped :initform (box nil
                           :copier #'identity
                           :name "Connection stopped box")
            :accessor %connection-stopped-p)
   (counters :initform (make-hash-table)
	     :reader connection-counters)))

(defun connection-stopped-p (connection)
  (box-value (%connection-stopped-p connection)))
(defun (setf connection-stopped-p*) (new-value connection)
  (setf (box-value (%connection-stopped-p connection)) new-value))

(defmethod initialize-instance :after ((connection connection) &key)
  (labels ((myself (message)
             "Try to avoid small timing issues with message handlers."
             (loop until (and (slot-boundp connection 'message-handler)
                              (not (eql (connection-message-handler connection)
                                        #'myself)))
                   do (sleep 0.1))
             (funcall (connection-message-handler connection) message)))
    (unless (slot-boundp connection 'message-handler)
      (setf (slot-value connection 'message-handler) #'myself))))

(defclass character-connection (connection) ()
  (:documentation "A connection that can send blocks with vectors of characters (strings) for data."))
(defmethod decentralise-messages:object->data ((object string)
                                               (connection character-connection)
                                               source)
  "Strings don't have to be translated further."
  (declare (ignore source))
  object)
(defmethod decentralise-messages:data->object ((data string)
                                               (connection character-connection)
                                               target)
  "Strings don't have to be translated further."
  (declare (ignore target))
  data)

(defclass binary-connection (connection) ()
  (:documentation "A connection that can send blocks with vectors of octets for data."))
(defmethod decentralise-messages:object->data ((object vector)
                                               (connection binary-connection)
                                               source)
  "Octet vectors don't have to be translated further." 
  (declare (ignore source))
  (if (equal (array-element-type object) '(unsigned-byte 8))
      object
      (call-next-method)))
(defmethod decentralise-messages:data->object ((data vector)
                                               (connection binary-connection)
                                               source)
  "Octet vectors don't have to be translated further." 
  (declare (ignore source))
  (if (equal (array-element-type data) '(unsigned-byte 8))
      data
      (call-next-method)))

;;; Counters

(defgeneric connection-counter-value (connection name)
  (:method ((connection connection) name)
    (values (gethash name (connection-counters connection) 0))))
(defgeneric (setf connection-counter-value) (new-value connection name)
  (:method (new-value (connection connection) name)
    (check-type new-value unsigned-byte)
    (setf (gethash name (connection-counters connection)) new-value)))
(defmacro with-connection-counter ((value connection name) &body body)
  (check-type connection symbol)
  `(let ((,value (connection-counter-value ,connection ',name)))
     (declare ((integer 0 *) ,value))
     (multiple-value-prog1
	 (progn ,@body)
       (incf (connection-counter-value ,connection ',name)))))

(defgeneric stop-connection (connection)
  (:method-combination progn)
  (:method :around ((connection connection))
    (unless (connection-stopped-p connection)
      (setf (connection-stopped-p* connection) t)
      (call-next-method)))
  (:method progn ((connection connection))
    "Run all the destructors on a connection."
    (dolist (destructor (connection-destructors connection))
      (funcall destructor connection))))
