(in-package :decentralise-connection)

(defvar *checked-connections* '()
  "A list of all the connections we're trying to find the element types of.")

(defgeneric connection-element-type (connection)
  (:documentation "Returns a type specifier for the type of block data that this connection can read or write.")
  (:method ((connection character-connection)) 'character)
  (:method ((connection binary-connection)) '(unsigned-byte 8))
  (:method :around ((connection connection))
    (if (member connection *checked-connections*)
        ;; If CONNECTION-ELEMENT-TYPE calls itself, we probably have infinite
        ;; recursion. The best type we can give is T.
        t
        (let ((*checked-connections* (cons connection *checked-connections*)))
          (call-next-method)))))
        
