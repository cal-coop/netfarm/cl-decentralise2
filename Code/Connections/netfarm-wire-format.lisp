(in-package :decentralise-connection)

(defclass netfarm-format-connection (socketed-connection character-connection) ())
(define-syntax netfarm-format-connection
  (:get ("get" &rest (names length-prefixed-string)))
  (:block ("block" (name length-prefixed-string) (version integer)
                   (lines integer)
                   &rest (channels length-prefixed-string))
    (let ((data (with-output-to-string (buffer)
                  (dotimes (n lines)
                    (write-string (read-line stream) buffer)
                    (unless (= n (1- lines))
                      (terpri buffer))))))
      (decentralise-messages:message :block name version channels data)))
  (:ok ("ok" (name length-prefixed-string)))
  (:error ("error" (name length-prefixed-string)
                   (reason length-prefixed-string)))
  (:allow-announcement ("announce" (announce? boolean)))
  (:announce ("node" (uri length-prefixed-string) (id length-prefixed-string)))
  (:subscribe ("subscribe" &rest (subscriptions length-prefixed-string)))
  (:subscription ("subscription" (name length-prefixed-string) (version integer)
                                 &rest (channels length-prefixed-string))))

(defun make-netfarm-format-connection (ip &rest initargs)
  (let* ((socket (usocket:socket-connect ip 1892 :element-type '(unsigned-byte 8)))
         (stream (usocket:socket-stream socket))
         (ssl-stream
           (cl+ssl:make-ssl-client-stream stream
                                          :hostname ip
                                          :external-format :utf-8)))
    (apply #'make-instance 'netfarm-format-connection
           :stream ssl-stream
           :socket socket
           initargs)))

(define-protocol netfarm 'make-netfarm-format-connection)
(defmethod connection-uri-protocol-name ((connection netfarm-format-connection))
  "netfarm")
