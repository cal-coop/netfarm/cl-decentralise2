(in-package :decentralise-connection)

(defclass socketed-connection (threaded-connection)
  ((stream :initarg :stream :reader connection-stream)
   (flush-buffer-every :initarg :flush-buffer-every :initform 1
                       :reader connection-flush-buffer-every)
   (unflushed-count :initform 0 :accessor connection-unflushed-count)
   (socket :initarg :socket :reader connection-socket)
   (address :initarg :address :initform nil :reader connection-address)))

(defmethod initialize-instance :after ((connection socketed-connection) &key)
  (when (null (connection-address connection))
    (setf (slot-value connection 'address)
          (format-ip
           (usocket:get-peer-address
            (connection-socket connection))))))

(defmethod read-message :around ((connection socketed-connection))
  (handler-case (call-next-method)
    (:no-error (message)
      (values message t))
    (end-of-file ()
      (stop-connection connection)
      (values nil nil))))

(defmethod stop-connection progn ((connection socketed-connection))
  (ignore-errors
   (usocket:socket-close (connection-socket connection))))

(defgeneric connection-uri-protocol-name (connection)
  (:documentation "Return the protocol name used for a connection URI, such as \"netfarm\"."))

(defun format-ip (ip)
  (ecase (length ip)
    (4  (usocket:vector-quad-to-dotted-quad ip))
    (16 (usocket:vector-to-ipv6-host ip))))

(defmethod connection-uri ((connection socketed-connection))
  (format nil "~a:~a"
          (connection-uri-protocol-name connection)
          (connection-address connection)))

(defmethod write-message :after ((connection socketed-connection) message)
  (when (= (incf (connection-unflushed-count connection))
           (connection-flush-buffer-every connection))
    (setf (connection-unflushed-count connection) 0)
    (finish-output (connection-stream connection))))

(defmacro with-connection-stream ((stream connection) &body body)
  (alexandria:once-only (connection)
    `(let ((,stream (connection-stream ,connection)))
       ,@body)))
