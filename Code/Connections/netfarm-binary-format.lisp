(in-package :decentralise-connection)

(defclass netfarm-binary-connection (socketed-connection
                                     binary-connection character-connection)
  ((maximum-block-length :initarg :maximum-block-length
                         :initform 10000000
                         :reader maximum-block-length)))

(defgeneric read-message-by-type-tag (connection type))
(defmethod read-message ((connection netfarm-binary-connection))
  (let* ((stream (with-connection-stream (stream connection) stream))
         (type-tag (read-byte stream)))
    (read-message-by-type-tag connection type-tag)))
(defmethod write-message :after ((connection netfarm-binary-connection) message)
  (declare (ignore message))
  (with-connection-stream (stream connection)
    (finish-output stream)))

(defmacro defwriter ((connection-name (message-tag &rest pattern) &optional type-tag)
                     &body body)
  (alexandria:with-gensyms (message stream)
    `(defmethod write-message ((,connection-name netfarm-binary-connection)
                               (,message
                                ,(decentralise-messages:message-type-specifier message-tag)))
       (with-connection-stream (,stream ,connection-name)
         (flet ((writer (byte)
                  (declare ((unsigned-byte 8) byte))
                  (write-byte byte ,stream)))
           (declare (inline writer)
                    (dynamic-extent #'writer))
           (decentralise-messages:message-case ,message
             ((,message-tag ,@pattern)
              ,(if (null type-tag)
                   nil
                   `(write-short-integer ,type-tag #'writer))
              ,@body)))))))

(defmacro defreader ((connection-name message-tag) &body body)
  (alexandria:with-gensyms (tag stream)
    `(defmethod read-message-by-type-tag ((,connection-name netfarm-binary-connection)
                                          (,tag (eql ,message-tag)))
       (with-connection-stream (,stream ,connection-name)
         (flet ((reader ()
                  (read-byte ,stream)))
           (declare (inline reader)
                    (dynamic-extent #'reader))
           ,@body)))))

;;; name-list ::= long-integer short-string*
(declaim (inline write-name-list read-name-list
                 write-metadata read-metadata))
(defun write-name-list (names writer)
  (write-long-integer (length names) writer)
  (dolist (name names)
    (write-short-string name writer)))
(defun read-name-list (reader)
  (let ((length (read-long-integer reader)))
    (loop repeat length
          collect (read-short-string reader))))
;;; metadata ::= short-string long-integer name-list
(defun write-metadata (name version channels writer)
  (write-short-string name writer)
  (write-long-integer version writer)
  (write-name-list channels writer))
(defun read-metadata (reader)
  (values (read-short-string reader)
          (read-long-integer reader)
          (read-name-list reader)))

;;; get ::= 01 name-list
(defwriter (connection (:get names) 1)
  (write-name-list names #'writer))
(defreader (connection 1)
  (decentralise-messages:message :get (read-name-list #'reader)))

;;; character-block ::= 02 metadata long-string
;;;    binary-block ::= 03 metadata long-bytes
(defwriter (connection (:block name version channels data))
  (etypecase data
    (string (write-short-integer 2 #'writer))
    (vector (write-short-integer 3 #'writer)))
  (write-metadata name version channels #'writer)
  (etypecase data
    (string (write-long-string data #'writer))
    (vector
     (unless (equal (array-element-type data) '(unsigned-byte 8))
       (setf data (coerce data '(vector (unsigned-byte 8)))))
     (write-long-bytes data #'writer))))
(defreader (connection 2)
  (let ((*maximum-vector-length* (maximum-block-length connection)))
    (multiple-value-bind (name version channels)
        (read-metadata #'reader)
      (decentralise-messages:message :block name version channels
                                     (read-long-string #'reader)))))
(defreader (connection 3)
  (let ((*maximum-vector-length* (maximum-block-length connection)))
    (multiple-value-bind (name version channels)
        (read-metadata #'reader)
      (decentralise-messages:message :block name version channels
                                     (read-long-bytes #'reader)))))

;;; ok ::= 04 short-string
(defwriter (connection (:ok name) 4)
  (write-short-string name #'writer))
(defreader (connection 4)
  (decentralise-messages:message :ok (read-short-string #'reader)))

;;; error ::= 05 short-string long-string
(defwriter (connection (:error name reason) 5)
  (write-short-string name #'writer)
  (write-long-string reason #'writer))
(defreader (connection 5)
  (decentralise-messages:message :error
                                 (read-short-string #'reader)
                                 (read-short-string #'reader)))

;;; subscribe ::= 06 name-list
(defwriter (connection (:subscribe new-subscriptions) 6)
  (write-name-list new-subscriptions #'writer))
(defreader (connection 6)
  (decentralise-messages:message :subscribe (read-name-list #'reader)))
;;; subscription ::= 07 metadata
(defwriter (connection (:subscription name version channels) 7)
  (write-metadata name version channels #'writer))
(defreader (connection 7)
  (multiple-value-bind (name version channels)
      (read-metadata #'reader)
    (decentralise-messages:message :subscription name version channels)))

;;;            boolean ::= 00 | 01
;;; allow-announcement ::= 08 boolean
(defwriter (connection (:allow-announcement allow?) 8)
  (write-short-integer (if allow? 1 0) #'writer))
(defreader (connection 8)
  (decentralise-messages:message :allow-announcement
                                 (= (read-short-integer #'reader) 1)))
;;; announce ::= 09 short-string short-string
(defwriter (connection (:announce uri id) 9)
  (write-short-string uri #'writer)
  (write-short-string id #'writer)) 
(defreader (connection 9)
  (decentralise-messages:message :announce
                                 (read-short-string #'reader)
                                 (read-short-string #'reader)))

(defun make-netfarm-binary-connection (ip &rest initargs)
  (let* ((socket (usocket:socket-connect ip 1893 :element-type '(unsigned-byte 8)))
         (stream (usocket:socket-stream socket))
         (ssl-stream (cl+ssl:make-ssl-client-stream stream
                                                    :hostname ip)))
    (apply #'make-instance 'netfarm-binary-connection
           :stream ssl-stream
           :socket socket
           initargs)))

(define-protocol nbinary 'make-netfarm-binary-connection)
(defmethod connection-uri-protocol-name ((connection netfarm-binary-connection))
  "nbinary")
