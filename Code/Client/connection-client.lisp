(in-package :decentralise-client)

(defclass connection-client (client)
  ((connection :initarg :connection :reader client-connection)
   (handler-table :initform (make-hash-table :test 'equal) :reader client-handler-table)
   (known-blocks :initform (make-hash-table :test 'equal) :reader client-known-blocks)
   (state-lock :initform (bt:make-recursive-lock "client table lock") :reader client-state-lock)
   (subscriptions :initform '() :initarg :channels :accessor client-subscriptions)
   (should-get-system-info :initarg :should-get-system-info :initform t
                           :reader client-should-get-system-info))
  (:documentation "A client that can take synchronous requests on behalf of a connection.
The connection used when instantiating a client should not be accessed by anything other than that client."))

(defmethod client-has-block-p ((client connection-client) name)
  (with-consistent-state (client)
    (cond
      ((not (client-should-get-system-info client)) nil)
      ((null (client-known-blocks client)) nil)
      (t (values (gethash name (client-known-blocks client)))))))

(defmethod count-known-blocks ((client connection-client))
  (with-consistent-state (client)
    (hash-table-count (client-known-blocks client))))

(defmethod update-known-block-info ((client connection-client) data)
  (flet ((update-known-block (name version channels)
           (declare (ignore channels))
           (setf (gethash name (client-known-blocks client))
                 version)))
    (if (stringp data)
        (map-listing #'update-known-block data)
        (map-binary-listing #'update-known-block data))))

(defmethod (setf client-subscriptions) :before (new-subscriptions (client connection-client))
  "Send over the set of channels designated by the client's subscription set."
  ;; Avoid sending another message if we didn't change the subscription set.
  (unless (null
           (set-exclusive-or new-subscriptions
                             (decentralise-client:client-subscriptions client)
                             :test #'string=))
    (decentralise-connection:subscribe (client-connection client) new-subscriptions)))

(defvar *in-consistent-state* '())
(defmethod call-with-consistent-state ((client connection-client) function)
  (bt:with-recursive-lock-held ((client-state-lock client))
    (let ((*in-consistent-state* (cons client *in-consistent-state*)))
      (funcall function))))

(defmethod handle-message ((client connection-client) message)
  (let ((name (message-name client message)))
    (with-consistent-state (client)
      (let ((callbacks (gethash name (client-handler-table client))))
        (remhash name (client-handler-table client))
        (loop for callback in callbacks
              do (funcall callback message))))))

(defmethod add-handler :before ((client connection-client) message-name function)
  (assert (member client *in-consistent-state*) ()
          "You do not have consistent state of ~s" client))
(defmethod add-handler ((client connection-client) message-name function)
  (push function (gethash message-name (client-handler-table client)))
  (values))
    
(defmethod send-message ((client connection-client) message)
  (decentralise-connection:write-message (client-connection client)
                                         message))
(defmethod map-subclients
    (function (client connection-client) message-name)
  (functional-action
   (lambda (success failure)
    (flet ((next-subclient ()
             (funcall failure (make-condition 'no-applicable-subclients
                                              :client client
                                              :name message-name))))
      (handler-bind ((error failure))
        (funcall function client success failure #'next-subclient))))))

(defgeneric glean-information-from-message (client message)
  (:method ((client connection-client) unknown)
    (declare (ignore unknown)))
  (:method ((client connection-client) (put decentralise-messages:put-block))
    (decentralise-messages:message-case put
      ((:block name ~ ~ data)
       (alexandria:switch (name :test #'string=)
         ("list" (update-known-block-info client data))))))
  (:method ((client connection-client) (subscription decentralise-messages:subscription))
    (decentralise-messages:message-case subscription
      ((:subscription name version ~)
       (setf (gethash name (client-known-blocks client)) version)))))

(defmethod initialize-instance :after ((client connection-client)
                                       &key (channels '("all")))
  (let ((connection (client-connection client)))
    (setf (decentralise-connection:connection-message-handler connection)
          (lambda (message)
            (glean-information-from-message client message)
            (handle-message client message)))
    (setf (decentralise-connection:connection-id connection)
          (parse-integer (get-data client "id") :radix 16))
    (when (client-should-get-system-info client)
      (decentralise-connection:subscribe (client-connection client)
                                         channels)
      (get-data client "list" :timeout 10))))

(defmethod stop-client :after ((client connection-client))
  (decentralise-connection:stop-connection (client-connection client)))

(defmethod print-object ((client connection-client) stream)
  (print-unreadable-object (client stream :type t :identity t)
    (let ((uri (decentralise-connection:connection-uri
                (client-connection client))))
      (unless (null uri)
        (write-string uri stream)))))
