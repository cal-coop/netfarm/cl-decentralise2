(in-package :decentralise-client)

(defgeneric get-block* (client name &key &allow-other-keys)
  (:documentation "Asynchronously retrieve the block named NAME from a client.")
  (:method :around ((client client) name &key)
    (let ((action (call-next-method)))
      (functional-action
       (lambda (success failure)
        (%run action
              success
              (lambda (condition)
                ;; If we receive NO-APPLICABLE-SUBCLIENTS, pretend it is
                ;; a "not found" :error message.
                (if (typep condition 'no-applicable-subclients)
                    (funcall failure
                             (make-condition 'remote-error
                                             :name name
                                             :message "not found"))
                    (funcall failure condition))))))))
  (:method ((client client) name &key)
    (do-subclients (subclient client `(:get ,name))
      (add-handler subclient `(:block ,name)
                   (lambda (message)
                     (decentralise-messages:message-case message
                       ((:block ~ name version channels)
                        (succeed (list name version channels))))))
      (add-handler subclient `(:error ,name)
                   (lambda (message)
                     (decentralise-messages:message-case message
                       ((:error ~ reason)
                        (if (string= reason "not found")
                            (next-subclient)
                            (fail (condition-from-message client message)))))))
      (send-message subclient
                    (decentralise-messages:message :get (list name))))))
(defun get-block (client name &rest rest &key timeout &allow-other-keys)
  (run (apply #'get-block* client name
              (alexandria:remove-from-plist rest :timeout))
       :timeout timeout))

(defun get-data (client name &rest arguments)
  (third (apply #'get-block client name arguments)))

(defgeneric put-block* (client name version channels data &key fail-if-too-old &allow-other-keys)
  (:documentation "Asynchronously send a block to a client.")
  (:method ((client client) name version channels data &key (fail-if-too-old nil))
    (do-subclients (subclient client `(:block ,name))
      (add-handler subclient `(:ok ,name)
                   (lambda (message)
                     (declare (ignore message))
                     (succeed t)))
      (add-handler subclient `(:error ,name)
                   (lambda (message)
                     (decentralise-messages:message-case message
                       ((:error ~ reason)
                        (if (and (string= reason "too old")
                                 (not fail-if-too-old))
                            (next-subclient)
                            (fail (condition-from-message client message)))))))
      (send-message subclient
                    (decentralise-messages:message :block
                     name version channels data)))))
(defun put-block (client name version channels data
                  &rest rest &key timeout &allow-other-keys)
  (run (apply #'put-block* client name version channels data
              (alexandria:remove-from-plist rest :timeout))
       :timeout timeout))
    
(defun connect-to-uri (uri &rest arguments
                           &key (class 'connection-client) &allow-other-keys)
  "Create a client that is connected to the given URI."
  (apply #'make-instance class
         :connection (decentralise-connection:connection-from-uri uri)
         (alexandria:remove-from-plist arguments :class)))
