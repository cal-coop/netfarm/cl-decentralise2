(defpackage :decentralise-client
  (:use :cl :decentralise-utilities :decentralise-binary-syntax)
  (:export #:client #:connection-client
           #:connect-to-uri
	   #:connect-to-system
           #:stop-client #:client-running-p
           #:call-with-consistent-state #:with-consistent-state
           #:message-name #:signal-error
           #:add-handler #:handle-message #:send-message #:map-subclients
           #:do-subclients #:succeed #:fail #:next-subclient
           #:get-block #:get-block* #:get-data #:put-block #:put-block*
           #:action
           #:run #:%run #:run-away
           #:then #:then* #:finish #:functional-action
           #:parallel #:attempt #:chain
           #:client-subscriptions
           #:count-known-blocks #:client-has-block-p
           #:client-connection #:client-subclients
           #:condition-from-message
           #:connection-closed #:remote-error #:request-timeout
           #:remote-error-message #:remote-error-name #:remote-error-case
           #:no-applicable-subclients))
