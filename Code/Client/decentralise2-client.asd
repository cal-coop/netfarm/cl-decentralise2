(asdf:defsystem :decentralise2-client
  :depends-on (:decentralise2-connections :decentralise2-systems :safe-queue)
  :serial t
  :components ((:file "package")
               (:file "conditions")
               (:file "protocol")
               (:file "actions")
               (:file "connection-client")
               (:file "client")
               (:file "direct-system")))
