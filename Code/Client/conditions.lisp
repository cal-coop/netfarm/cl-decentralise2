(in-package :decentralise-client)

(define-condition connection-closed (error)
  ((connection :initarg :connection :reader connection-closed-connection))
  (:report (lambda (condition stream)
             (format stream "The connection ~s was closed" (connection-closed-connection condition)))))

(define-condition remote-error (error)
  ((message :initarg :message :reader remote-error-message)
   (name    :initarg :name    :reader remote-error-name))
  (:report (lambda (condition stream)
             (format stream "~s: ~a"
                     (remote-error-name condition)
                     (remote-error-message condition)))))

(define-condition no-applicable-subclients (error)
  ((name   :initarg :name   :reader no-applicable-subclients-name)
   (client :initarg :client :reader no-applicable-subclients-client))
  (:report (lambda (condition stream)
             (format stream "No applicable subclients were found for ~s on ~s."
                     (no-applicable-subclients-name condition)
                     (no-applicable-subclients-client condition)))))

(define-condition request-timeout (error)
  ())

(defmacro remote-error-case (form &body handlers)
  "Evaluate FORM with handlers set up for some remote error messages."
  (alexandria:with-gensyms (e block)
    `(block ,block
       (decentralise-utilities:handler-case* ,form
         (remote-error (,e)
           (alexandria:switch ((remote-error-message ,e) :test #'string=)
             ,@(loop for (message variable . body) in handlers
                     collect `(,message
                               (let ,(if (null variable)
                                         '()
                                         `((,(first variable) ,e)))
                                 (return-from ,block (progn ,@body)))))))))))
       
