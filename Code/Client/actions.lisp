(in-package :decentralise-client)

(defclass action ()
  ()
  (:documentation "The protocol class for an action, which represents something that can be done asynchronously."))

(defun run (action &key timeout)
  "Run an action, returning its return value, or signalling an error it produced."
  (let ((mailbox (safe-queue:make-mailbox)))
    (%run action
          (lambda (value)
            (safe-queue:mailbox-send-message mailbox
                                             (cons t value)))
          (lambda (condition)
            (safe-queue:mailbox-send-message mailbox
                                             (cons nil condition))))
    (let ((value (safe-queue:mailbox-receive-message mailbox
                                                     :timeout timeout)))
      (cond
        ((null value)                   ; Timeout
         (error 'request-timeout))
        ((car value) (cdr value))       ; Success
        (t (error (cdr value)))))))     ; Failure

(defun run-away (action)
  "Run an action, without blocking for its value."
  (%run action (constantly nil) (constantly nil)))

(defgeneric %run (action success failure)
  (:documentation "Implement RUN by setting up the action to be run somewhere, and either call SUCCESS with a value, or FAILURE with a condition. This probably should not block.")
  #+trace-actions
  (:method :around (action success failure)
    (format *debug-io* "~&=> ~a" action)
    (call-next-method
     action
     (lambda (v)
       (format *debug-io* "~&<= ~a succeeded with ~a" action v)
       (funcall success v))
     (lambda (v)
       (format *debug-io* "~&<= ~a failed with ~a" action v)
       (funcall failure v)))))

(defclass chained-action (action)
  ((first-action :initarg :first    :reader first-action)
   (producer     :initarg :producer :reader producer)))
(defmethod %run ((action chained-action) success failure)
  (%run (first-action action)
        (lambda (value)
          (%run (funcall (producer action) value) success failure))
        failure))

(defun then (action action-producer)
  "Produce an action which runs the first action, then calls the action-producer to get a second action, and run that.
This is a lookalike of >>="
  (make-instance 'chained-action
                 :first action
                 :producer action-producer))
(defun then* (action1 action2)
  "Produce an action which runs the first action, then runs the second action.
This is a lookalike of >>"
  (then action1 (constantly action2)))

(defclass finish-action (action)
  ((value :initarg :value :reader value)))
(defmethod %run ((action finish-action) success failure)
  (funcall success (value action)))

(defun finish (value)
  "Produce an action that always returns a value."
  (make-instance 'finish-action :value value))

(defclass functional-action (action)
  ((function :initarg :function :reader action-function)))
(defmethod %run ((action functional-action) success failure)
  (handler-case
      (funcall (action-function action) success failure)
    (error (e)
      (funcall failure e))))
(defun functional-action (function)
  "Produce an action that calls a function with success and failure continuations."
  (make-instance 'functional-action :function function))

(defmacro chain (&body body)
  "Chain some requests together with THEN. This looks like `do' notation from Haskell, but the symbol DO was taken. And it's probably not a mooooo-nad."
  (if (alexandria:length= 1 body)
      (first body)
      (destructuring-bind ((variable <- value) &body body) body
        (alexandria:eswitch (<- :test #'string= :key #'symbol-name)
          ("<-"
           `(then ,value
                  (lambda (,variable)
                    (declare (ignorable ,variable))
                    (chain ,@body))))
          ("="
           `(let ((,variable ,value)) (chain ,@body)))))))

(defun parallel (actions)
  (case (length actions)
    (0 (return-from parallel (finish #())))
    (1 (return-from parallel
         (chain
          (a <- (elt actions 0))
          (finish (vector a))))))
  (let* ((lock (bt:make-lock "PARALLEL action result lock"))
         (failed? nil)
         (success-count 0)
         (length (length actions))
         (results (make-array length)))
    (functional-action
     (lambda (success failure)
      (loop for action in (coerce actions 'list)
            for position from 0
            do (%run action
                     (let ((position position))
                       (lambda (value)
                         (bt:with-lock-held (lock)
                           (setf (svref results position) value)
                           (when (= (incf success-count) length)
                             (funcall success results)))))
                     (lambda (condition)
                       (bt:with-lock-held (lock)
                         ;; If we already failed, don't bother continuing
                         ;; with the failure continuation again.
                         (unless failed?
                           (setf failed? t)
                           (funcall failure condition))))))))))

(defun attempt (action producer)
  "Attempt to run ACTION, but if that fails, call PRODUCER with the condition to get an action, and run that instead."
  (functional-action
   (lambda (success failure)
    (%run action success
          (lambda (c)
            (%run (funcall producer c) success failure))))))
