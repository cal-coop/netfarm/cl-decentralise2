(in-package :decentralise-standard-system)

;;; Block metadata is stored in two tables, and request generation uses a
;;; queue of block names.
;;; Metadata for interesting objects is stored in the "interesting block table",
;;; and for uninteresting objects is stored in the "uninteresting block table",
;;; which are fairly fitting names.

;;; The "interesting block queue" is a queue with names of blocks that are very
;;; likely to be requestable from a connection. Names are enqueued by
;;; %ADD-KNOWN-BLOCK and (successful or unsuccessful) completion of requests,
;;; and GET-NEXT-REQUEST-NAME will dequeue names.

(declaim (optimize (speed 3) (safety 1) (debug 1)))
(defstruct block-info
  connection
  (version 0 :type (unsigned-byte 64))
  channels)

(defmacro with-block-info ((connection version channels) info &body body)
  (alexandria:once-only (info)
    `(let ((,connection (block-info-connection ,info))
           (,version    (block-info-version    ,info))
           (,channels   (block-info-channels   ,info)))
       ,@body)))

(defgeneric add-known-blocks (system connection blocks)
  (:documentation "Add information about blocks we heard about to the system.")
  (:method ((system standard-system) connection blocks)
    (map 'nil
         (lambda (block)
           (destructuring-bind (name version channels)
               block
             (add-known-block system connection name version channels)))
         blocks)))

(declaim (inline value-unless-null partition))
(defun value-unless-null (value)
  (if (null value)
      (values nil   nil)
      (values value t)))

(defun partition (predicate list)
  "Return a list of every item in LIST that satisfies PREDICATE, and a list of every item that does not."
  (declare (function predicate))
  (loop for item in list
        if (funcall predicate item)
          collect item into truthy
        else
          collect item into falsy
        finally (return (values truthy falsy))))

;;; Block information is stored in a table, sorted with newest blocks first
;;; in the table.
(declaim (inline insert-block-info* insert-block-info))
(defun insert-block-info* (info list)
  "Insert information for a block into a list of block information objects."
  (declare (optimize (speed 3) (safety 1)))
  (let* ((collection (list '.dummy.))
         (version (block-info-version info))
         (connection (block-info-connection info))
         (tail collection))
    (declare (cons collection tail)
             (decentralise-connection:connection connection)
             (optimize (safety 1) (space 0) (debug 0) (compilation-speed 0)))
    ;; If the list is empty, the inserted list is the singleton list of the
    ;; given information.
    (when (null list)
      (return-from insert-block-info* (list info)))
    (loop for cons on list
          for (first . nil) = cons
          ;; If the rest of the list is older than this information, then
          ;; put this information and the rest of the list on the end, and
          ;; get out.
          when (> version (block-info-version first))
            do (setf (cdr tail) (cons info cons))
               (return-from insert-block-info* (rest collection))
          unless (eq connection (block-info-connection first))
            ;; Otherwise, just keep going. Also, drop information about newer
            ;; versions should they exist.
            do (let ((new-pair (list first)))
                 (setf (cdr collection) new-pair
                       tail new-pair)))
    ;; All of the other blocks are newer than us.
    (setf (cdr tail) (list info))
    (rest collection)))
(defun insert-block-info (connection version channels list)
  "Insert information for a block into a list of block information objects."
  (declare ((unsigned-byte 64) version)
           (optimize (speed 3) (safety 1)))
  (insert-block-info* (make-block-info :connection connection
                                       :version version
                                       :channels channels)
                      list))

(defun cut-versions-before (minimum-version list)
  (loop for info in list
        until (> minimum-version (block-info-version info))
        collect info))

(defun enqueue-block-name (system name)
  (safe-queue:mailbox-send-message
   (system-interesting-blocks-mailbox system)
   (vector name)))

(defvar *enqueue-function* #'enqueue-block-name)

(defvar *buffer-size* 100)
(defun call-with-names-buffer (system function)
  (let ((buffer          (make-array *buffer-size*))
        (buffer-position 0)
        (mailbox (system-interesting-blocks-mailbox system)))
    (flet ((write-to-buffer (system name)
             (declare (ignore system))
             (when (= buffer-position (length buffer))
               (safe-queue:mailbox-send-message mailbox
                                                buffer)
               (setf buffer-position 0
                     buffer (make-array *buffer-size*)))
             (setf (aref buffer buffer-position) name)
             (incf buffer-position)))
      (let ((*enqueue-function* #'write-to-buffer))
        (funcall function)
        (safe-queue:mailbox-send-message
         mailbox
         (subseq buffer 0 buffer-position))))))

(defmacro with-names-buffer ((system) &body body)
  `(call-with-names-buffer ,system (lambda () ,@body)))

;;; Updating a system for a changed interesting object predicate
(defgeneric move-information-between-tables
    (system name from-table to-table interesting?)
  (:method ((system standard-system) name from-table to-table interesting?)
    (declare (function *enqueue-function*))
    (let (move)
      (modify-value (name from-table)
          (infos present?)
        (declare (ignore present?))
        (multiple-value-bind (move* keep)
            (partition (lambda (info)
                         (with-block-info (connection version channels)
                             info
                           (declare (ignore connection))
                           ;; test if the generalised booleans are equal
                           (not
                            (alexandria:xor (interesting-block-p system name
                                                                 version channels)
                                            interesting?))))
                       infos)
          (setf move move*)
          (value-unless-null keep)))
      (modify-value (name to-table)
          (infos present?)
        (declare (ignore present?))
        (let ((infos infos))
          (dolist (new-info move)
            (setf infos (insert-block-info* new-info infos)))
          (value-unless-null infos)))
      ;; Enqueue if it's now interesting.
      (when (and interesting?
                 (not (null move)))
        (funcall *enqueue-function* system name)))))

(defun chash-table-keys (hash-table)
  (let ((keys '()))
    (do-concurrent-table (key value hash-table)
      (push key keys)
      (values value t))
    keys))

(defgeneric move-information-by-set-designator
    (system set-designator from-table to-table interesting?)
  (:method ((system standard-system) (all (eql t))
            from-table to-table interesting?)
    "Check every known block." 
    (dolist (name (chash-table-keys from-table))
      (move-information-between-tables system name
                                       from-table to-table interesting?)))
  (:method ((system standard-system) (set list)
            from-table to-table interesting?)
    "Check every known block in the list SET."
    (dolist (name set)
      (when (nth-value 1 (getchash name from-table))
        (move-information-between-tables system name
                                         from-table to-table interesting?))))
  (:method ((system standard-system) (predicate function)
            from-table to-table interesting?)
    "Check every known block whose name satisfies PREDICATE."
    (dolist (name (chash-table-keys from-table))
      (when (funcall predicate name)
        (move-information-between-tables system name
                                         from-table to-table
                                         interesting?)))))

(defgeneric update-system-for-new-interesting-block-predicate
    (system interesting-set uninteresting-set)
  (:documentation "Update bookkeeping in SYSTEM to accomodate changes in INTERESTING-OBJECT-P.
Both INTERESTING-SET and UNINTERESTING-SET can be a list of changed block statuses, a predicate satisfied by block names that should be reconsidered, or T if all known blocks should be reconsidered.
This is likely one of the longest symbols you will ever type.")
  (:method ((system standard-system) interesting-set uninteresting-set)
    (let ((interesting (system-interesting-blocks system))
          (uninteresting (system-uninteresting-blocks system)))
      (with-names-buffer (system)
        (move-information-by-set-designator system interesting-set
                                            uninteresting interesting
                                            t)
        (move-information-by-set-designator system uninteresting-set
                                            interesting uninteresting
                                            nil)))))

(defgeneric add-known-block (system connection name version channels)
  (:documentation "Add information about a block we heard about to the system.")
  (:method ((system standard-system) connection name version channels)
    (declare (function *enqueue-function*))
    (cond
      ((interesting-block-p system name version channels)
       (modify-value (name (system-interesting-blocks system))
           (infos present?)
         (declare (ignore present?))
         (values (insert-block-info connection version channels infos)
                 t))
       (funcall *enqueue-function* system name))
      ((uninteresting-block-p system name version channels)
       (modify-value (name (system-uninteresting-blocks system))
           (infos present?)
         (declare (ignore present?))
         (values (insert-block-info connection version channels infos)
                 t))))))

(define-time-meter *remove-old-info-meter* "Removing superseded sources"
                   "standard-system")
(defgeneric remove-old-info (system name maximum-version)
  (:documentation "Remove information about any versions of a block named NAME not newer than MAXIMUM-VERSION, which we don't need.")
  (:method ((system standard-system) name maximum-version)
    (with-time-meter (*remove-old-info-meter*)
      (dolistit ((table
                  (system-interesting-blocks system)
                  (system-uninteresting-blocks system)))
        (modify-value (name table)
            (infos present?)
          (if present?
              (value-unless-null (cut-versions-before maximum-version infos))
              (values nil nil)))))))

(defgeneric remove-connection (system connection)
  (:documentation "Remove information about a now disconnected connection.")
  (:method ((system standard-system) connection)
    "Remove all known blocks from the connection."
    (dolistit ((table
                (system-interesting-blocks system)
                (system-uninteresting-blocks system)))
      (do-concurrent-table (name values table)
        (declare (ignore name))
        (value-unless-null (remove connection values
                                   :key #'block-info-connection))))))

(defun pop-connection-for-name (system name)
  "Pop a connection for getting the block NAME from the system SYSTEM."
  (let ((info nil)
        version)
    (multiple-value-bind (info present?)
        (getchash name (scheduler-current-requests system))
      (if present?
          (setf version (block-info-version info))
          (setf version -1)))
    (modify-value (name (system-interesting-blocks system))
        (infos present?)
      (if (not present?)
          (values nil nil)
          (destructuring-bind (first . rest) infos
            (cond
              ((> (block-info-version first) version)
               (setf info first)
               (value-unless-null rest))
              (t
               (values infos t))))))
    (if (null info)
        nil
        (block-info-connection info))))

(defun connections-for-name-p (system name)
  "Test if there are connections for getting a block."
  (not (null (getchash name (system-interesting-blocks system)))))
