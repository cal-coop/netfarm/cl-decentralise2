(in-package :decentralise-standard-system)

(defgeneric interesting-block-p (system name version channels)
  (:documentation "Determine if a block should be synchronised now, based on its metadata.

See UPDATE-SYSTEM-FOR-NEW-INTERESTING-BLOCK-PREDICATE
See UNINTERESTING-BLOCK-P")
  (:method ((system standard-system) name version channels)
    "The default method considers all objects interesting."
    (declare (ignore name version channels))
    t))

(defgeneric uninteresting-block-p (system name version channels)
  (:documentation "Determine if a block would ever be interesting.")
  (:method ((system standard-system) name version channels)
    (declare (ignore channels))
    (decentralise-system:new-version-p system name version)))
