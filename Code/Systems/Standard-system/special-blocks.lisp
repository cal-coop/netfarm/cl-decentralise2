(in-package :decentralise-standard-system)
(declaim (optimize (speed 3) (safety 1) (debug 1)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro define-simple-error (name &optional (string (substitute #\Space #\- (string-downcase name))))
    `(progn
       (define-condition ,name (error) ())
       (defmethod decentralise-system:describe-decentralise-error ((error ,name))
         ,string))))

(define-time-meter *get-meter* "Reading blocks" "standard-system")
(define-time-meter *put-meter* "Saving blocks" "standard-system")
(define-simple-error too-old)

(#+sbcl sb-ext:defglobal
 #+ccl ccl:defstaticvar
 #-(or ccl sbcl) defvar *block-names* (make-hash-table :test 'equal))
(#+sbcl sb-ext:defglobal
 #+ccl ccl:defstaticvar
 #-(or ccl sbcl) defvar *maximum-name-length* 0)
(defgeneric special-put (system connection name real-name version channels data)
  (:argument-precedence-order name system connection real-name version channels data)
  (:method ((system standard-system) connection name real-name version channels data)
    (with-time-meter (*put-meter*)
      (unless (decentralise-system:new-version-p system real-name version)
        (error 'too-old))
      (decentralise-system:put-block
       system real-name version channels
       (data->object data connection
                     (system-translation-target system)))
      (distribute-after-put-block system real-name version channels)
      (remove-old-info system real-name version))))
(defgeneric special-get (system connection name real-name)
  (:argument-precedence-order name system connection real-name)
  (:method ((system standard-system) connection name real-name)
    (with-time-meter (*get-meter*)
      (multiple-value-bind (version channels data)
          (decentralise-system:get-block system real-name)
        (values version
                channels
                (object->data data connection
                              (system-translation-target system)))))))

(define-simple-error reserved-block)
(defun fallback (&rest rest)
  (declare (ignore rest))
  (error 'reserved-block))

(defmacro define-special-block ((class-name block-name
                                 &optional (data-type-name 't))
                                &key get put)
  "Define a special block (a block that has some computation involved, such as listing blocks from the backend) and GET and PUT methods on it. If a method isn't provided, a fallback is used that writes the error \"reserved block\"."
  (when (symbolp block-name)
    (setf block-name (string-downcase block-name)))
  (let ((name-length (length block-name))
        (identifier (gensym "IDENTIFIER")))
    `(progn
       (when (> ,name-length *maximum-name-length*)
         (setf *maximum-name-length* ,name-length))
       (let ((,identifier (or (gethash ',block-name *block-names*)
                              (setf (gethash ',block-name *block-names*)
                                    ',block-name))))
         (declare (ignorable ,identifier))
         ,(unless (null put)
            `(defmethod special-put ((system ,class-name)
                                     connection (name (eql ,identifier))
                                     real-name version channels data)
               (declare (ignore real-name))
               (funcall ,put system connection version channels
                        (data->object data connection ',data-type-name))))
         ,(unless (null get)
            `(defmethod special-get ((system ,class-name)
                                     connection
                                     (name (eql ,identifier)) real-name)
               (declare (ignore real-name))
               (multiple-value-bind (version channels data)
                   (funcall ,get system connection)
                 (values version channels (object->data data connection ',data-type-name)))))))))
(indent:define-indentation define-special-block (4 &body))

(declaim (inline find-special-block-name))
(defun find-special-block-name (name)
  "In some implementations, hashing might run quite slow with long names. This function only tries to GETHASH if the name is short enough to be a special block name."
  (declare (string name)
           (fixnum *maximum-name-length*)
           (hash-table *block-names*))
  (if (> (length name) *maximum-name-length*)
      nil
      (values (gethash name *block-names*))))

(define-special-block (standard-system "nodes")
  :get (lambda (system this-connection)
	 (decentralise-connection:with-connection-counter (n this-connection nodes)
	   (values n '()
		   (with-output-to-string (buffer)
		     (with-unlocked-box (connections
                                         (decentralise-system:system-connections system))
		       (loop for connection in connections
			     unless (or (not (decentralise-connection:connection-announcing-p connection))
					(null (decentralise-connection:connection-id connection))
                                        (null (decentralise-connection:connection-uri connection))
					(eq connection this-connection))
			       do (write-nodes-line
                                   (decentralise-connection:connection-uri connection)
                                   (decentralise-connection:connection-id connection)
                                   buffer)))))))
  ;; TODO: This is useful for peer discovery, which we don't implement yet.
  ;; Should we? Should this be another mixin?
  :put (lambda (system connection version channels data)
         (declare (ignore system connection version channels))
         (handler-case (map-nodes (constantly nil) data)
           (error () (error 'invalid-listing))
           (:no-error (nodes)
             (declare (ignore nodes))
             '|I caren't|))))
