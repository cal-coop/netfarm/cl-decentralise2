(defpackage :decentralise-standard-system
  (:use :cl :decentralise-utilities :decentralise-messages
        :decentralise-binary-syntax :concurrent-hash-table)
  (:export #:standard-system
           #:define-simple-error #:define-special-block
           #:update-system-for-new-interesting-block-predicate
           #:interesting-block-p #:uninteresting-block-p
           #:connection-interested-in-p #:broadcast-metadata
           #:distribute-after-put-block #:add-block-information #:too-old
           #:system-synchronisation-channels 
           #:connection-information-storage-mixin
           #:system-translation-target
           #:map-known-blocks-of-connection #:remove-connection
           #:with-consistent-block #:with-consistent-blocks))
