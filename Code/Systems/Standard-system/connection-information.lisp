(in-package :decentralise-standard-system)

(defclass connection-information-storage-mixin ()
  ())

(defmethod decentralise-system:populate-connection-information
    ((system standard-system) connection information)
  (setf (gethash 'known-blocks information)
        (make-hash-table :test 'equal))
  (setf (gethash 'channel-set information)
        (channel-list->channel-set '() 'hash-table-channel-set)))

;;; Known blocks

(defgeneric add-known-block-to-connection
    (system connection name version channels)
  (:method ((system standard-system) connection name version channels)
    (declare (ignore connection name version channels)))
  (:method ((system connection-information-storage-mixin)
            connection name version channels)
    (decentralise-system:with-connection-value (table system connection known-blocks)
      (setf (gethash name table) (cons version channels)))))
(defgeneric map-known-blocks-of-connection
    (function system connection)
  (:method (function (system connection-information-storage-mixin) connection)
    (decentralise-system:with-connection-value (table system connection known-blocks)
      (maphash (lambda (name info)
                 (destructuring-bind (version . channels) info
                   (funcall function name version channels)))
               table))))

;;; Connection channel sets

(defgeneric (setf connection-channel-set) (new-set system connection)
  (:method (new-set (system standard-system) connection)
    (setf (decentralise-system:connection-value system connection 'channel-set)
          new-set)))
(defgeneric connection-channel-set (system connection)
  (:method ((system standard-system) connection)
    (decentralise-system:connection-value system connection 'channel-set)))
