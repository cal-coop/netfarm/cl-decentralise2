(in-package :decentralise-standard-system)

(defun standard-system-message-dispatcher (connection system message)
  (decentralise-messages:message-case message
    ((:block name version channels data)
     (handler-case* (special-put system connection
                                 (find-special-block-name name)
                                 name version channels data)
       (error (e)
         (decentralise-connection:write-error
          connection name
          (decentralise-system:describe-decentralise-error e)))
       (:no-error (&rest r)
         (declare (ignore r))
         (decentralise-connection:write-ok connection name)))
     ;; We may need to check for better sources to retrieve from. For
     ;; example, if a better source is found while retriving from
     ;; another source, the former would otherwise be ignored by the
     ;; scheduler.
     (remove-request system connection name)
     (when (connections-for-name-p system name)
       (enqueue-block-name system name)))
    ((:get names)
     (loop until (null names)
           do (let ((batch
                      (loop repeat 10
                            for name = (pop names)
                            until (null name)
                            collect name)))
                (with-consistent-blocks (batch system)
                  (loop for name in batch
                        collect
                        (handler-case* 
                         (special-get system connection
                                      (find-special-block-name name)
                                      name)
                         (error (e)
                           (decentralise-messages:message :error
                            name
                            (decentralise-system:describe-decentralise-error e)))
                         (:no-error (version channels data)
                           (decentralise-messages:message :block
                            name version channels data)))
                          into messages
                        finally (decentralise-connection:write-messages connection messages))))))
    ((:allow-announcement announce-p)
     (if (and announce-p (null (decentralise-connection:connection-id connection)))
         (decentralise-connection:write-error connection "announce" "need id first")
         (setf (decentralise-connection:connection-announcing-p connection)
               announce-p)))
    ((:subscribe subscriptions)
     (setf (connection-channel-set system connection)
           (channel-list->channel-set subscriptions
                                      'hash-table-channel-set))
     (with-unlocked-box (connections (system-subscribed-connections system))
       (if (null subscriptions)
           (setf connections (remove connection connections))
           (pushnew connection connections))))
    ((:subscription name version channels)
     (add-block-information system connection name version channels))
    ((:error name ~)
     ;; We could get an :error message if we requested a block that somehow
     ;; doesn't exist anymore.
     (remove-request system connection name)
     (when (connections-for-name-p system name)
       (enqueue-block-name system name)))))
