(in-package :decentralise-standard-system)

(declaim (optimize (speed 3) (safety 1) (debug 1)))

(defconstant +starting-sleep-time+ 0.00001)
(defconstant +maximum-sleep-time+  0.01)

(defmethod decentralise-system:leader-loop ((system standard-system))
  ;; The leader thread schedules new requests for connections.
  ;; Connection handlers used to do this themselves, but we found it faster
  ;; to let the leader handle it while a connection was busy processing its
  ;; request. This may change if we have a large number of connections,
  ;; but right now we have the leader perform exponential backoff to wait
  ;; for when it is appropriate to add more requests; it starts waiting 10µs
  ;; for an opening, then waits 1.5× longer each time until it waits for
  ;; 10ms. Signalling a semaphore is even quite slow compared to scheduling,
  ;; so "guessing" when we will want another request works strangely well.
  ;; We also delete requests that timed out every 50ms.
  (let ((last-sleep-time +starting-sleep-time+)
        (*current-chunk* #())
        (*current-position* 0))
      (loop
        (when (decentralise-system:system-stopping-p system)
          (return))
        (cond
          ((should-add-requests-p system)
           (setf last-sleep-time +starting-sleep-time+)
           (add-generated-requests system))
          (t
           ;; We only consider removing requests if we really can't
           ;; make any other progress seemingly. While it appears
           ;; inefficient to wait so long, the timeout length is going
           ;; to be magnitudes larger than the whole exponential
           ;; backoff schedule.
           (when (= last-sleep-time +maximum-sleep-time+)
             (remove-old-requests system))
           (unless (should-add-requests-p system)
             (sleep last-sleep-time)
             (setf last-sleep-time
                   (min (* last-sleep-time 1.5)
                        +maximum-sleep-time+))))))))

(defmethod decentralise-system:start-system :after ((system standard-system)))

(defmethod decentralise-system:add-connection :after
    ((system standard-system) connection)
  (setf (decentralise-connection:connection-message-handler connection)
        (lambda (message)
          (standard-system-message-dispatcher connection system message)))
  (push (lambda (connection)
          (remove-connection system connection))
        (decentralise-connection:connection-destructors connection)))

(defgeneric system-synchronisation-channels (system)
  (:method ((system standard-system))
    '("all")))

(defmethod decentralise-system:connect-system :after
    ((system standard-system) (connection decentralise-connection:connection))
  ;; This ordering is very important. If the next two lines were switched,
  ;; then new blocks could appear between the listing and subscription and
  ;; there would be a semi-permanent gap in system knowledge.
  (decentralise-connection:subscribe connection
                                     (system-synchronisation-channels system))
  (decentralise-connection:request-block connection "list")
  (decentralise-connection:request-block connection "id"))

(defgeneric system-translation-target (system)
  (:method ((system standard-system)) 't))
