(in-package :decentralise-standard-system)

(defclass channel-set ()
  ()
  (:documentation "A protocol class for a channel set, which represents the set of channels a connection is subscribed to."))

(defgeneric channel-list->channel-set (list target-class)
  (:documentation "Create a channel set of the target class from the given list of channels."))

(defgeneric channel-set-interested-p (channel-set channels)
  (:documentation "Are any of the channels a member of the channel set?"))
