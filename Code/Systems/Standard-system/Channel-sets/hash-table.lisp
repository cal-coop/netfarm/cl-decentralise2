(in-package :decentralise-standard-system)

(defclass hash-table-channel-set (channel-set)
  ((all? :initarg :all? :reader hash-table-channel-set-all?)
   (channels :initarg :channels :reader hash-table-channel-set-channels)))

(defmethod channel-list->channel-set (list (class (eql 'hash-table-channel-set)))
  (let ((channels (make-hash-table :test 'equal))
        (all? nil))
    (dolist (channel list)
      (if (and (not all?)
               (string= channel "all"))
          (setf all? t)
          (setf (gethash channel channels) t)))
    (make-instance 'hash-table-channel-set :all? all? :channels channels)))

(defmethod channel-set-interested-p
    ((channel-set hash-table-channel-set) channels)
  (or (hash-table-channel-set-all? channel-set)
      (loop with table = (hash-table-channel-set-channels channel-set)
            for channel in channels
            thereis (gethash channel table))))
