(in-package :decentralise-standard-system)

(defmacro with-consistent-block ((name system) &body body)
  "Run BODY in an environment where the state of the system with respect to the block named BLOCK is internally consistent."
  `(with-named-resource (,name (block-consistency-locks ,system))
     ,@body))

(defmacro with-consistent-blocks ((names system) &body body)
  "Run BODY in an environment where the state of the system with respect to all blocks named in BLOCKS are internally consistent."
  `(with-named-resources (,names (block-consistency-locks ,system))
     ,@body))
