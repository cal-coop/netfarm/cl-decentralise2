------------------------------- MODULE queue -------------------------------
EXTENDS Naturals, TLC, Sequences, FiniteSets
CONSTANTS connections

  (*-- The aim of this model is to verify that the decentralise2 scheduler
       will still work with its very fine-grained locking strategy. We
       set off some connections to race to install information and provide
       different versions of a block. All connections can spuriously fail.
       
       We expect only one request to be running at a time, and for the system
       to always find the best version which doesn't fail. --*)

max(S) == IF S = {}
          THEN 0
          ELSE CHOOSE x \in S: \A y \in S: y <= x

valid_succeeding == SUBSET (1..connections)

(*--algorithm Queue

variables running = {}, \* Which sources are we running from?
          information = {}, \* Which versions do we have data for?
          enqueued = 0, \* How many times is the block in the queue?
          succeeding \in valid_succeeding,
          expected_version = max(succeeding),
          version = 0;

procedure enqueue_update(n = 1)
  variables more_information = FALSE;
begin
\* We lock to do the actual update.
Succeed:
  if n > version then
    version := n;
  end if;
StopRunning:
  running := running \ {n};
\* Re-schedule if we see there is still information.
Load:
  \* As the information is really stored in a list this load is indeed atomic.
  more_information := information /= {};
Reschedule:
  if more_information then
    enqueued := enqueued + 1;
  end if;
  return;
end procedure;

fair process enqueue \in 1..connections
begin
InstallInformation:
  information := information \union {self};
Enqueue:
  enqueued := enqueued + 1;
ProvideInformation:
\* We simplify the proof by stopping the world when we get to the best version.
  await version = expected_version \/ self \in running;
  if version < expected_version then
    if self \in succeeding then
      call enqueue_update(self);
    else
      SpuriouslyFail:
        running := running \ {self};
      Retry:
        enqueued := enqueued + 1;
    end if;
  end if;
end process

fair process scheduler = 0
variables chosen_source = 0;
begin
Dequeue:
  await enqueued > 0 \/ version = expected_version;
  if version = expected_version then
    goto Done;
  else
    enqueued := enqueued - 1;
  end if;
ReadRunning:
\* We wait for the old request to complete.
  if running /= {} then
    goto Dequeue;
  end if;
ReadInformation:
\* Again, this bit is implemented as an atomic update on the interesting block table.
\* A 'spurious' queueing could happen.
  if information = {} then
    goto Dequeue;
  else
    chosen_source := max(information);
    information := information \ {chosen_source};
  end if;
Schedule:
\* We actually just send a message to the connection in question.
  running := running \union {chosen_source};
  goto Dequeue;
end process

end algorithm;
*)

EventuallyLoadsBest == <>(version = expected_version)

OnlyOneRunning == Cardinality(running) <= 1

=============================================================================
\* Modification History
\* Last modified Sat Aug 14 16:37:21 AEST 2021 by hayley
\* Created Wed Aug 11 20:51:01 AEST 2021 by hayley