(in-package :decentralise-standard-system)

(defclass standard-system (decentralise-system:system)
  ((scheduler-timeout :initarg :timeout :initform 10 :reader scheduler-timeout)
   (scheduler-concurrent-requests :initarg :concurrent-requests
                                  :initform 8000 :reader scheduler-requests)
   (current-requests :initform (decentralise-system:make-block-table
                                :name "Current request table")
                     :accessor scheduler-current-requests)
   (block-consistency-locks :initform (make-named-resource-table)
                            :reader block-consistency-locks)
   (interesting-blocks-mailbox :initform (safe-queue:make-mailbox)
                               :reader system-interesting-blocks-mailbox)
   (interesting-blocks :initform (decentralise-system:make-block-table
                                  :name "Interesting blocks")
                       :reader system-interesting-blocks)
   (janitor-mailbox :initform (safe-queue:make-mailbox)
                    :reader system-janitor-mailbox)
   (leader-request-semaphore :initform (bt:make-semaphore)
                             :reader system-leader-semaphore)
   (subscribed-connections :initform (box '())
                           :reader system-subscribed-connections)
   (uninteresting-blocks :initform (decentralise-system:make-block-table
                                    :name "Uninteresting blocks")
                         :reader system-uninteresting-blocks)))
