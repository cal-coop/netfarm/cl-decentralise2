(in-package :decentralise-standard-system)

(declaim (optimize (speed 3) (safety 1) (debug 1)))

(defvar *current-chunk*)
(defvar *current-position*)
(defvar *additional-planned* 0)

(defgeneric should-add-requests-p (system)
  (:method ((system standard-system))
    (and (or (< *current-position* (length *current-chunk*))
             (not (safe-queue:mailbox-empty-p
                   (system-interesting-blocks-mailbox system))))
         (< (+ *additional-planned*
               (chash-table-count (scheduler-current-requests system)))
            (scheduler-requests system)))))

(defgeneric get-next-request-name (system)
  (:method ((system standard-system))
    (cond
      ((< *current-position* (length *current-chunk*))
       (prog1 (svref *current-chunk* *current-position*)
         (incf *current-position*)))
      (t
       (let ((next-chunk
               (safe-queue:mailbox-receive-message
                (system-interesting-blocks-mailbox system)
                :timeout 0.1)))
         (when (null next-chunk)
           (return-from get-next-request-name nil))
         (setf *current-chunk* next-chunk
               *current-position* 0)
         (get-next-request-name system))))))

(defgeneric get-next-request (system)
  (:documentation "Retrieve the next block name to retrieve, and a connection to retrieve it from.")
  (:method ((system standard-system))
    (let ((name (get-next-request-name system)))
      (if (null name)
          (values nil nil)
          (let ((connection (pop-connection-for-name system name)))
            (if (null connection)
                (values nil  nil)
                (values name connection)))))))

(define-time-meter *remove-old-requests* "Clearing old requests" "standard-system")
(defgeneric remove-old-requests (system)
  (:method ((system standard-system))
    ;; All the timed out requests were made (now - timeout) seconds ago, so
    ;; we remove and re-schedule all the requests that are at least that old.
    (let ((latest-time (- (get-internal-real-time)
                          (* (scheduler-timeout system) internal-time-units-per-second)))
          (interesting-blocks (system-interesting-blocks system))
          (interesting-block-mailbox (system-interesting-blocks-mailbox system))
          (names-to-retry '()))
      (with-time-meter (*remove-old-requests*)
        (do-concurrent-table (name request (scheduler-current-requests system))
          ;; This could be re-invoked depending on the CHT implementation, so
          ;; reset the list.
          (setf names-to-retry '())
          (unless (null request)
            (destructuring-bind (connection time)
                request
              (declare (ignore connection))
              (cond
                ((<= time latest-time)
                 ;; Enqueue the block again for retrieval, if there are still
                 ;; other sources for it.
                 (when (getchash name interesting-blocks)
                   (push name names-to-retry))
                 (values nil nil))
                (t (values request t)))))))
      (dolist (name names-to-retry)
        (safe-queue:mailbox-send-message interesting-block-mailbox name)))))

(define-time-meter *generate-requests* "Generating requests" "standard-system")
(defgeneric add-generated-requests (system)
  (:method ((system standard-system))
    ;; If we schedule many requests at the same time, we can minimise
    ;; the messages sent out by requesting multiple blocks in one
    ;; GET-BLOCKS message. We exploit the chunks that are generated
    ;; already when adding new interesting blocks, exploiting the
    ;; common case of receiving many block names provided by one
    ;; connection.
    (let ((current-connection nil)
          (current-names '())
          (request-table (scheduler-current-requests system))
          (*additional-planned* 0))
      (flet ((flush (new-connection)
               (unless (null current-connection)
                 (ignore-errors
                  (decentralise-connection:request-blocks
                   current-connection current-names)
                  (let ((time (get-internal-real-time)))
                    (dolist (name current-names)
                      (setf (getchash name request-table)
                            (list current-connection time))))))
               (setf current-connection new-connection
                     current-names '()
                     *additional-planned* 0)))
        (with-time-meter (*generate-requests*)
          (loop while (should-add-requests-p system)
                do (multiple-value-bind (name connection)
                       (get-next-request system)
                     (when (null name)
                       (loop-finish))
                     (unless (eq connection current-connection)
                       (flush connection))
                     (incf *additional-planned*)
                     (push name current-names)))
          (flush nil))))))

(defgeneric remove-request (system connection name)
  (:method ((system standard-system) connection name)
    (modify-value (name (scheduler-current-requests system))
        (info present?)
      (if present?
          (if (eq (first info) connection)
              (values nil nil)
              (values info t))
          (values nil nil)))))
