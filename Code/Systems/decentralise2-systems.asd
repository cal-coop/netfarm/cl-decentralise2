(asdf:defsystem :decentralise2-systems
  :depends-on (:decentralise2-utilities :decentralise2-connections
               :decentralise2-acceptors
               :trivial-indent
               :alexandria :trivial-backtrace :trivial-garbage)
  :serial t
  :components ((:file "package")
               (:file "protocol")
               (:file "echo")
               (:file "memory-database-mixin")
               (:file "define-system")
               (:file "connection-information")
               (:module "Standard-system"
                :components ((:file "package")
                             (:file "block-consistency")
                             (:file "class")
                             (:file "protocol")
                             (:file "message-dispatch")
                             (:module "Channel-sets"
                              :components ((:file "protocol")
                                           (:file "hash-table")))
                             (:file "connection-information")
                             (:file "special-blocks")
                             (:file "known-blocks")
                             (:file "requests")
                             (:file "channels")
                             (:file "system-setup")
                             (:module "Inbuilt-special-blocks"
                              :components ((:file "list")
                                           (:file "id")))))))
