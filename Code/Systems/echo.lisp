(in-package :decentralise-system)

(defclass echo-system (system) ())

(defmethod leader-loop ((system echo-system))
  (loop
    (when (system-stopping-p system)
      (return-from leader-loop))
    (sleep 0.1)))

(defmethod add-connection :after ((system echo-system) connection)
  (setf (decentralise-connection:connection-message-handler connection)
        (lambda (message)
          (decentralise-connection:write-message connection message))))
