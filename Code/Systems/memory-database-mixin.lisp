(in-package :decentralise-system)

(defmacro specialize (string body)
  "Convince the compiler to generate a fast path for simple strings."
  `(if (typep ,string '(simple-array character 1))
       ,body
       ,body))

(defun djb (string)
  (declare (string string)
           (optimize speed))
  (let ((hash 5381))
    (declare ((and unsigned-byte fixnum) hash))
    (specialize
     string
     (dotimes (n (length string))
       (setf hash
             (logand most-positive-fixnum
                     (logxor (* hash 33)
                             (char-code (schar string n)))))))
    hash))

(defun simple-string-hash (string)
  (declare (string string)
           (optimize speed))
  (let ((value 0))
    (declare ((unsigned-byte 32) value))
    (specialize
     string
     (loop for char across string
           for position below 2
           do (setf value (logxor value (char-code char)))))
    value))

(defun make-block-table (&key (name "a block table"))
  (concurrent-hash-table:make-chash-table
   :size 10000
   :test #'equal
   :hash-function #'djb
   :name name
   :segment-hash-function #'simple-string-hash))

(defclass memory-database-mixin ()
  ((data-table :initform (make-block-table)
               :reader memory-database-data-table)))

(defmethod get-block ((system memory-database-mixin) name)
  (multiple-value-bind (value win)
      (concurrent-hash-table:getchash name (memory-database-data-table system))
    (if win
        (destructuring-bind (version channels data) value
          (values version channels data))
        (error 'not-found))))

(define-time-meter *write-to-memory-database* "Write to memory database" "system")
(defmethod put-block ((system memory-database-mixin) name version channels data)
  (with-time-meter (*write-to-memory-database*)
    (setf (concurrent-hash-table:getchash name
                                          (memory-database-data-table system))
          (list version channels data))))

(defmethod map-blocks (function (system memory-database-mixin))
  (declare ((function ((unsigned-byte 64) list t)) function))
  (concurrent-hash-table:mapchash
      (lambda (name data)
        (destructuring-bind (version channels data) data
          (declare (ignore data))
          (funcall function name version channels)))
      (memory-database-data-table system)))

(defun memory-database-size (system)
  (concurrent-hash-table:chash-table-count
   (memory-database-data-table system)))
