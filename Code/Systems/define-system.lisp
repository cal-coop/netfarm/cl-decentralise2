(in-package :decentralise-system)

(defmacro make-acceptor (name &rest arguments)
  `(make-instance ',name
                  ,@(loop for (key value) on arguments by #'cddr
                          appending `(,key ',value)))) 

(defmacro define-system (name (&rest classes)
                         &rest arguments &key (acceptors '()) &allow-other-keys)
  "Create a system 'declaratively', by providing a list of superclasses and acceptors, and other keyword arguments to initialise the system."
  `(defvar ,name
     (make-instance
      (make-instance 'standard-class
                     :direct-superclasses (list ,@(loop for class in classes
                                                        collect `(find-class ',class))))
      :acceptors (list ,@(loop for acceptor-specs in acceptors
                               collect `(make-acceptor ,@acceptor-specs)))
    ,@(alexandria:remove-from-plist arguments :acceptors))))
