(in-package :decentralise-system)

(defun %connection-information (system connection)
  (concurrent-hash-table:getchash connection
                                  (system-connection-information system)))

(defmacro with-connection-information-held ((information system connection) &body body)
  `(with-unlocked-box (,information
                       (%connection-information ,system ,connection))
     ,@body))

(defun (setf connection-value) (new-value system connection name)
  (with-connection-information-held (table system connection)
    (setf (gethash name table) new-value)))
(defun connection-value (system connection name)
  (with-connection-information-held (table system connection)
    (gethash name table)))
(defmacro with-connection-value ((value system connection name) &body body)
  (alexandria:with-gensyms (table)
    `(with-connection-information-held (,table ,system ,connection)
       (symbol-macrolet ((,value (gethash ',name ,table)))
         ,@body))))

(defgeneric make-connection-information (system connection)
  (:method ((system system) connection)
    (let ((information (make-hash-table :test 'eql)))
      (populate-connection-information system connection information)
      information)))

(defgeneric populate-connection-information (system connection information)
  (:method ((system system) connection information)
    (declare (ignore connection information))))

(defgeneric add-connection-information (system connection)
  (:method ((system system) connection)
    (setf (concurrent-hash-table:getchash connection
                                          (system-connection-information system))
          (box (make-connection-information system connection)))))
(defgeneric remove-connection-information (system connection)
  (:method ((system system) connection)
    (concurrent-hash-table:remchash connection
                                    (system-connection-information system))))
