(asdf:defsystem :decentralise2-messages
  :components ((:file "package")
               (:file "message-case")
               (:file "messages")
               (:file "data-translation")))
