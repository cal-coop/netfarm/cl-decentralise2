(in-package :decentralise-acceptor)

(defclass acceptor ()
  ((system :reader system)))

(defgeneric start-acceptor (acceptor system)
  (:method :before ((acceptor acceptor) system)
    (setf (slot-value acceptor 'system) system)))

(defclass threaded-acceptor (acceptor)
  ((connector-thread :initform nil
                     :reader connector-thread)))

(defgeneric give-system-connection (system connection))

(defgeneric accept-connection (acceptor))

(defmethod start-acceptor ((acceptor threaded-acceptor) system)
  (setf (slot-value acceptor 'connector-thread)
        (with-thread (:name (format nil "decentralise2 ~a"
                                    (type-of acceptor)))
          (acceptor-loop acceptor))))

(defgeneric acceptor-loop (acceptor)
  (:method ((acceptor threaded-acceptor))
    (loop
      (handler-case
          (let ((connection (accept-connection acceptor)))
            (give-system-connection (system acceptor) connection))
        (error (e)
          (format *debug-io* "*** ~a~%" e))))))

(defgeneric stop-acceptor (acceptor)
  (:method ((acceptor acceptor)))
  (:method ((acceptor threaded-acceptor))
    (handler-case
        (bt:destroy-thread (connector-thread acceptor))
      (error (e)
        (declare (ignore e))
        (error "Acceptor is already stopped")))))
