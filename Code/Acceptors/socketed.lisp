(in-package :decentralise-acceptor)

(defclass socket-acceptor (threaded-acceptor)
  ((server-socket :accessor server-socket)
   (host :initform "0.0.0.0" :initarg :host :reader acceptor-host)
   (port :initform 1892 :initarg :port :reader acceptor-port)
   (certificate :initform (alexandria:required-argument :certificate)
                :initarg :certificate :reader acceptor-certificate)
   (key :initform (alexandria:required-argument :key)
        :initarg :key :reader acceptor-key)
   (connection-class :initarg :connection-class
                     :initform (alexandria:required-argument 'connection-class)
                     :reader connection-class)))

(defun connection-class-element-type (class)
  (closer-mop:finalize-inheritance class)
  (cond
    ((subtypep class 'decentralise-connection:binary-connection)
     '(unsigned-byte 8))
    ((subtypep class 'decentralise-connection:character-connection)
     'character)
    (t (error "~s is not a subclass of BINARY-CONNECTION or CHARACTER-CONNECTION"
              class))))

(defmethod start-acceptor :before ((acceptor socket-acceptor) system)
  (let* ((connection-class (connection-class acceptor))
         (connection-class (etypecase connection-class
                             (class  connection-class)
                             (symbol (find-class connection-class))))
         (element-type (connection-class-element-type connection-class)))
    (setf (server-socket acceptor)
          (usocket:socket-listen (acceptor-host acceptor)
                                 (acceptor-port acceptor)
                                 :element-type element-type
                                 :backlog 64
                                 :reuse-address t))))

(defmethod stop-acceptor :before ((acceptor socket-acceptor))
  "Close the listening socket when we're done with it"
  (ignore-errors
    (usocket:socket-close (server-socket acceptor))))

(defmethod accept-connection ((acceptor socket-acceptor))
  (let* ((socket (usocket:socket-accept (server-socket acceptor)))
         (name (usocket:get-peer-name socket))
         (port (usocket:get-peer-port socket))
         (stream (cl+ssl:make-ssl-server-stream (usocket:socket-stream socket)
                                                :external-format :utf-8
                                                :certificate (acceptor-certificate acceptor)
                                                :key (acceptor-key acceptor)))
         (connection (make-instance (connection-class acceptor)
                                    :socket socket :stream stream)))
    ;; These probably belong in a log somewhere.
    #+(or)
    (format *debug-io* "~a:~d connected~%" name port)
    #+(or)
    (push (lambda (connection)
            (declare (ignore connection))
            (format *debug-io* "~a:~d disconnected~%" name port))
          (decentralise-connection:connection-destructors connection))
    connection))
