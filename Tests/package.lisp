(defpackage :decentralise-tests
  (:use :cl
        :decentralise-system :decentralise-standard-system
        :decentralise-acceptor :decentralise-connection
        :decentralise-utilities
        :parachute)
  (:export #:run-tests #:gitlab-ci-test))
(in-package :decentralise-tests)
