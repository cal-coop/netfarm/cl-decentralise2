(in-package :decentralise-tests)

(defclass noisy-interactive (interactive plain)
  ())

(defun run-tests (&optional interactive?)
  (eql (status
	(test
         '(synchronisation system-semantics client-tests kademlia-client)
         :report (if interactive? 'noisy-interactive 'plain)))
       :passed))

(defun gitlab-ci-test ()
  (or (run-tests)
      (uiop:quit -1)))
