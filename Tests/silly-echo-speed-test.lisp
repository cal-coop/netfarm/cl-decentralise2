(defun silly-echo-speed-test (uri &key (messages 1000))
  (let ((connection (decentralise-connection:connection-from-uri uri))
        (lock (bt:make-lock))
        (count 0))
    (setf (decentralise-connection:connection-message-handler connection)
          (lambda (message)
            (assert (tree-equal message '(:get ("foo")) :test #'equal))
            (bt:with-lock-held (lock)
              (incf count))))
    (unwind-protect
         (let ((start-time (get-internal-real-time)))
           (dotimes (x messages)
             (decentralise-connection:request-block connection "foo"))
           (loop while (< count messages)
                 do (sleep 0.01))
           (let* ((end-time (get-internal-real-time))
                  (delta (/ (- end-time start-time) internal-time-units-per-second)))
             (format t "~d message~:p in ~$ second~:p, ~f message~:p/second~%"
                     messages
                     delta
                     (/ messages delta))))
      (format t "received ~d messages~%" count)
      (decentralise-connection:stop-connection connection))))
