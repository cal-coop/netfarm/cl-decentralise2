(in-package :decentralise-tests)

(define-test synchronisation)
;;; To my knowledge, sb-sprof does not work on SBCL on Windows. 
#+(and :sbcl (not :windows) (not :gitlab-ci))
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'sb-sprof))

(defun random-string (&key (length 8)
                           (bag "1234567890abcdefghijklmnopqrstuvwxyz")
                           (state (random (expt 2 64)))
                           (prefix ""))
  (declare ((unsigned-byte 64) state)
           ((unsigned-byte 8) length)
           (string prefix bag)
           (optimize (speed 3)))
  (flet ((next-random (size)
           "An implementation of the MWC64X random number generator. I had an implementation of it lying around already."
           (let ((c (ldb (byte 32 32) state))
                 (x (ldb (byte 32 0)  state)))
             (setf state (ldb (byte 64 0) (+ c (* x 4294883355))))
             (mod (logxor c x) size))))
    (let* ((prefix-length (length prefix))
           (string (make-string (+ length prefix-length))))
      (replace string prefix)
      (dotimes (position length)
        (setf (char string (+ position prefix-length))
              (char bag (next-random (length bag)))))
      (values string state))))

(defvar *table-header* "    time   blocks  threads  waiting in-flight")
(defun print-statistics (time system)
  (format *debug-io* "~&~8,2f ~8d ~8d ~8d ~8d~%"
          time
          (memory-database-size system)
          (length (bt:all-threads))
          (safe-queue:mailbox-count
           (decentralise-standard-system::system-interesting-blocks-mailbox system))
          (concurrent-hash-table:chash-table-count
           (decentralise-standard-system::scheduler-current-requests
            system))))

(defun watch-systems (target-system source-systems)
  (loop with source-size = (reduce #'+ source-systems
                                   :key #'memory-database-size)
        with start-time = (get-internal-real-time)
        for time = (/ (- (get-internal-real-time) start-time)
                      internal-time-units-per-second)
        for last-time = -1 then time
        for current-size = (memory-database-size target-system)
        for print-row? = (< (mod time 1/5) 1/100)
        until (> time 20)
        if (= current-size source-size)
          do (format *debug-io* "~&Everything synchronised in ~$ second~:p!~%"
                     time)
             (return t)
        else do (when print-row?
                  (print-statistics time target-system))
                (sleep 0.01)
        finally (return nil)))

(defun wait-for-synchronisation-to-start (system1 system2)
  (declare (ignore system1))
  (format *debug-io* "~&Waiting for synchronisation to start...~%")
  (loop for time from 0.0 to 20.0 by 0.01
        if (zerop (memory-database-size system2))
          do (sleep 0.01)
        else
          do (format *debug-io* "~&Synchronisation started after ~$ second~:p~%" time)
             (loop-finish)))

(defun populate-system (system &key (prefix "") (count 100000))
  (let ((state (random (expt 2 64))))
    (declare ((unsigned-byte 64) state))
    (dotimes (n count)
      (when (zerop (mod n 10000))
        (write-char #\.))
      (multiple-value-bind (name next-state)
          (random-string :state state :prefix prefix)
        (setf state next-state)
        (decentralise-system:put-block system
                                       name 1 '() name)))))

(define-test synchronises-everything
  :parent synchronisation
  (let ((system1 (make-instance 'test-system :timeout 20))
        (system2 (make-instance 'test-system :timeout 20)))
    (start-system system1)
    (start-system system2)
    ;; Populate system1's data table.
    (populate-system system1 :count 100000)
    (decentralise-utilities:reset-meters '("system" "standard-system"))
    (attach-systems system1 system2)
    (format *debug-io* "~&Synchronising ~d item~:p~%"
            (memory-database-size system1))
    (unwind-protect
         (progn
           (wait-for-synchronisation-to-start system1 system2)
           (write-line *table-header* *debug-io*)
           (true (watch-systems system2 (list system1))
                 "The synchronisation test failed."))
      (decentralise-utilities:print-meters '("standard-system" "system"))
      (stop-system system1)
      (stop-system system2))))

(define-test synchronises-everything/2
  :parent synchronisation
  (let ((source1 (make-instance 'test-system
                                :timeout 20))
        (source2 (make-instance 'test-system
                                :timeout 20))
        (target (make-instance 'test-system
                               :timeout 20)))
    (start-system source1)
    (start-system source2)
    (start-system target)
    ;; Populate the data tables of the source systems.
    (populate-system source1 :prefix "1" :count 50000)
    (populate-system source2 :prefix "2" :count 50000)
    (decentralise-utilities:reset-meters '("system" "standard-system"))
    (attach-systems source1 target)
    (attach-systems source2 target)
    (format *debug-io* "~&Synchronising ~d item~:p~%"
            (+ (memory-database-size source1)
               (memory-database-size source2)))
    (unwind-protect
         (progn
           (wait-for-synchronisation-to-start source1 source2)
           (write-line *table-header* *debug-io*)
           (true (watch-systems target (list source1 source2))
                 "The synchronisation test failed."))
      (decentralise-utilities:print-meters '("standard-system" "system"))
      (stop-system source1)
      (stop-system source2)
      (stop-system target))))
