(in-package :decentralise-tests)

(defclass test-kademlia-format () ())
(defclass test-kademlia-system
    (test-kademlia-format decentralise-kademlia:kademlia-system-mixin test-system)
  ())
(defclass test-kademlia-client
    (test-kademlia-format decentralise-kademlia:kademlia-client)
  ())
(defmethod decentralise-kademlia:parse-hash-name ((kademlia test-kademlia-format) name)
  (ignore-errors (parse-integer name)))

(defvar *blocks*
  '(("1" 1 () "block one, accessible through *A*")
    ("2" 1 () "block two, accessible through *C*, which is accessible through *B*")
    ("3" 1 () "block three, accessible through *B*, which is accessible through *A*")
    ("4" 1 () "block four, accessible through *D*, which is accessible through *B*")))

;;         A(1) -> B(3) -> D(4)
;;                  |
;;                  V
;;                 C(2)

(define-network (kademlia-test-network :system-class test-kademlia-system)
  (*a* :connected-to (*b*)     :initargs (:id 1 :distance 0) :blocks (list (nth 0 *blocks*)))
  (*b* :connected-to (*c* *d*) :initargs (:id 3 :distance 0) :blocks (list (nth 2 *blocks*)))
  (*c* :connected-to ()        :initargs (:id 2 :distance 0) :blocks (list (nth 1 *blocks*)))
  (*d* :connected-to ()        :initargs (:id 4 :distance 0) :blocks (list (nth 3 *blocks*))))

(define-test kademlia-client)

(defun reachable-p (client name)
  (handler-case (decentralise-client:get-block client name)
    (error () nil)
    (:no-error (&rest values)
      (declare (ignore values))
      t)))

(define-test everything-is-reachable
  :parent kademlia-client
  (start-kademlia-test-network)
  (unwind-protect
       (let ((client (make-instance 'test-kademlia-client
                                    :bootstrap-uris '("test:*a*"))))
         (unwind-protect
              (progn
                (true (reachable-p client "1"))
                (true (reachable-p client "2"))
                (true (reachable-p client "3"))
                (true (reachable-p client "4"))
                (false (reachable-p client "5")))
           (decentralise-client:stop-client client)))
    (stop-kademlia-test-network)))
