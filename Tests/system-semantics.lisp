(in-package :decentralise-tests)

(define-test system-semantics)

;; An accessor for messing with the interesting object predicate.
(defgeneric interesting-object-predicate (system))
(defvar *now-interesting-set* t)
(defvar *now-uninteresting-set* t)
(defgeneric (setf interesting-object-predicate) (new-predicate system)
  (:method :after (new-predicate (system standard-system))
    (unless (system-stopped-p system)
      (update-system-for-new-interesting-block-predicate system
                                                         *now-interesting-set*
                                                         *now-uninteresting-set*))))

(defclass very-mutable-system (test-system)
  ((interesting-object-predicate :initarg :interesting-object-predicate
                                 :initform (alexandria:required-argument
                                            :interesting-object-predicate)
                                 :accessor interesting-object-predicate)))

(defmethod interesting-block-p ((system very-mutable-system) name version channels)
  (funcall (interesting-object-predicate system) name version channels))

(define-network (simple-update-iop-network :system-class very-mutable-system)
  (*all-knowing-node* :blocks '(("a" 0 () "Block A")
                                ("b" 0 () "Block B"))
                      :initargs (:interesting-object-predicate (constantly t)))
  (*curious-node* :connected-to (*all-knowing-node*)
                  :initargs (:interesting-object-predicate (constantly nil))))

(defun clear-storage (system)
  "Clear the 'persistent' storage from a system."
  (concurrent-hash-table:do-concurrent-table
      (key value (decentralise-system::memory-database-data-table system))
    (declare (ignore key value))
    (values nil nil)))

(defun wait-for-system-to-be-affected ()
  (sleep 0.5))
    
(define-test update-iop
  :parent system-semantics
  (clear-storage *curious-node*)
  (setf (interesting-object-predicate *curious-node*) (constantly nil))
  (with-simple-update-iop-network ()
    (wait-for-system-to-be-affected)
    (is = 0 (memory-database-size *curious-node*)
        "The curious node should not have retrieved anything, with the i-o-p = (CONSTANTLY NIL)")
    (setf (interesting-object-predicate *curious-node*)
          (lambda (name version channels)
            (declare (ignore version channels))
            (string= name "a")))
    (wait-for-system-to-be-affected)
    (is = 1 (memory-database-size *curious-node*)
        "The curious node should have retrieved exactly one block, while only interested in block A")
    (setf (interesting-object-predicate *curious-node*)
          (constantly t))
    (wait-for-system-to-be-affected)
    (is = 2 (memory-database-size *curious-node*)
        "The curious node should have retrieved both blocks, with the i-o-p = (CONSTANTLY T)")))
