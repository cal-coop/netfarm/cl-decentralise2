(asdf:defsystem :decentralise2-tests
  :depends-on (:decentralise2 :alexandria :parachute :uiop)
  :components ((:file "package")
               (:file "run-tests")
               (:file "test-uri")
               (:file "synchronisation")
               (:file "system-semantics")
               (:file "kademlia")
               (:file "client")))
