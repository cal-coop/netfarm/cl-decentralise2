#+sbcl (setf sb-ext:*derive-function-types* t)
(push :gitlab-ci *features*)
(ql:quickload :decentralise2-tests)
(decentralise-tests:gitlab-ci-test)
